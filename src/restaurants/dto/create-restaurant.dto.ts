import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, Length } from 'class-validator';

import { STATUSES } from './../../utils/constants';

export class CreateRestaurantDto {
  @ApiProperty({
    example: 'testRestaurant',
    required: true,
  })
  @IsNotEmpty({ message: 'Please enter restaurant name' })
  name: string;

  @ApiProperty({
    example: 'testRestaurant description here',
    required: false,
  })
  description: string;

  @ApiProperty({
    example: '0.00-5.00',
    required: false,
  })
  rating: number;

  @ApiProperty({
    example: '12.345678',
    required: false,
  })
  latitude: number;

  @ApiProperty({
    example: '12.345678',
    required: true,
  })
  longitude: number;

  @ApiProperty({
    example: 50,
    required: true,
  })
  capacity: number;

  @ApiProperty({
    enum: STATUSES,
    example: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
    required: false,
  })
  status: string;

  @ApiProperty({
    example: 'testRestaurant',
    required: true,
  })
  @IsNotEmpty({ message: 'Please enter username' })
  username: string;

  @ApiProperty({
    example: 'myPowefulPa$$001',
    required: true,
  })
  @IsNotEmpty({ message: 'Please enter password' })
  @Length(6, 50, { message: 'Password length Must be between 6 and 50 charcters' })
  password: string;
}
