import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Param,
  Patch,
  Query,
  Request,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';

import {
  AdminAccess,
  EmployeeAccess,
  OrganizationAccess,
  RestaurantAccess,
  UserAccess,
} from './../decorators/guard.decorator';
import { validatePagination, validateSortOrder, validateStatus } from './../utils/functions';
import { SORT_ORDER, STATUSES } from './../utils/constants';

import { RestaurantsService } from './restaurants.service';
// import { CreateRestaurantDto } from './dto/create-restaurant.dto';
import { UpdateRestaurantDto } from './dto/update-restaurant.dto';
import { Restaurant } from './entities/restaurant.entity';

const prefix = 'restaurants';

@ApiTags(prefix)
@ApiResponse({ status: 401, description: 'Unauthorized.' })
@UseInterceptors(ClassSerializerInterceptor)
@Controller(prefix)
export class RestaurantsController {
  constructor(private readonly restaurantsService: RestaurantsService) {}

  // @Post()
  // create(@Body() createRestaurantDto: CreateRestaurantDto) {
  //   return this.restaurantsService.create(createRestaurantDto);
  // }

  @AdminAccess()
  @EmployeeAccess()
  @RestaurantAccess()
  @OrganizationAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The records has been successfully fetched.' })
  @ApiQuery({
    name: 'status',
    required: false,
    type: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'contracted',
    required: false,
    type: Boolean,
  })
  @ApiQuery({
    name: 'page',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: `${Object.values(SORT_ORDER).filter((v) => isNaN(Number(v)))}`,
  })
  @Get()
  findAll(
    @Request() req,
    @Query('status') status: string | STATUSES,
    @Query('contracted') contracted: boolean,
    @Query() options: IPaginationOptions,
    @Query('sortOrder') sortOrder: SORT_ORDER,
  ): Promise<Pagination<Restaurant>> {
    validatePagination(options, prefix);
    sortOrder = validateSortOrder(sortOrder);
    status = validateStatus(status);

    return this.restaurantsService.findAll(
      status,
      contracted,
      options,
      sortOrder,
      req.user.id,
      req.user.type,
    );
  }

  @AdminAccess()
  @EmployeeAccess()
  @RestaurantAccess()
  @OrganizationAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @Get(':id')
  findOne(@Request() req, @Param('id') id: string) {
    return this.restaurantsService.findOne(+id, req.user.id, req.user.type);
  }

  @RestaurantAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully updated.' })
  @ApiBody({
    type: UpdateRestaurantDto,
    description: 'Json structure for restaurant object',
  })
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateRestaurantDto: UpdateRestaurantDto) {
    return this.restaurantsService.update(+id, updateRestaurantDto);
  }
}
