import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { IPaginationOptions, Pagination, paginate, paginateRaw } from 'nestjs-typeorm-paginate';

import { SORT_ORDER, STATUSES, USER_TYPES } from './../utils/constants';
import { passwordMd5Encrypt } from './../utils/functions';

// import { CreateRestaurantDto } from './dto/create-restaurant.dto';
import { UpdateRestaurantDto } from './dto/update-restaurant.dto';
import { Restaurant } from './entities/restaurant.entity';

@Injectable()
export class RestaurantsService {
  constructor(@InjectRepository(Restaurant) private restaurantRepository: Repository<Restaurant>) {}

  async findAll(
    status: string | STATUSES,
    contracted: boolean,
    options: IPaginationOptions,
    sortOrder: SORT_ORDER,
    userId: string,
    type: string,
  ): Promise<Pagination<Restaurant>> {
    const queryBuilder = this.restaurantRepository.createQueryBuilder('r');

    if (status) {
      queryBuilder.andWhere({ status: status });
    }

    queryBuilder.orderBy('r.name', sortOrder).getRawMany();

    if (type == USER_TYPES.ORGANIZATION) {
      queryBuilder
        .leftJoin('r.contracts', 'roc')
        .addSelect(
          `SUM(IF(roc.organization_id = ${userId} AND roc.status = 1 AND roc.restaurant_status = 1 AND roc.organization_status = 1, 1, 0))`,
          'contracted',
        )
        .groupBy('r.id');

      if (contracted != undefined) {
        if (contracted) {
          queryBuilder.having('contracted > 0');
        } else {
          queryBuilder.having('contracted = 0');
        }
      }
      const raw = await paginateRaw<Restaurant>(queryBuilder, options);
      const entities = await paginate<Restaurant>(queryBuilder, options);

      for (let i = 0; i < entities.items.length; i++) {
        entities.items[i].contracted = raw.items[i].contracted;
      }
      return entities;
    } else {
      return paginate<Restaurant>(queryBuilder, options);
    }
  }

  async findOne(id: number, userId: string, type: string): Promise<Restaurant> {
    const queryBuilder = this.restaurantRepository.createQueryBuilder('r');

    if (type == USER_TYPES.ORGANIZATION) {
      queryBuilder
        .leftJoin('r.contracts', 'roc')
        .addSelect(
          `SUM(IF(roc.restaurant_id = ${userId} AND roc.status = 1 AND roc.restaurant_status = 1 AND roc.organization_status = 1, 1, 0))`,
          'contracted',
        )
        .andWhere({ id: id });
    } else {
      queryBuilder.andWhere({ id: id });
    }

    const restaurant = await queryBuilder.getOne();

    if (!restaurant) throw new UnauthorizedException('error.restaurant not found');

    if (type == USER_TYPES.ORGANIZATION) {
      const restaurantRaw = await queryBuilder.getRawOne();
      restaurant.contracted = restaurantRaw['contracted'];
    }

    return restaurant;
  }

  async findOneByUsername(username: string, statuses?: STATUSES[]): Promise<Restaurant> {
    let condition = null;

    if (statuses) {
      condition = { status: In(statuses) };
    } else {
      condition = { status: Object.values(STATUSES)[STATUSES.ACTIVE] };
    }

    const restaurant = await this.restaurantRepository.findOne({
      where: {
        username: username,
        ...condition,
      },
    });

    if (!restaurant) throw new UnauthorizedException('error.restaurant not found');

    return restaurant;
  }

  async findOneById(userId: string, statuses?: STATUSES[]): Promise<Restaurant> {
    let condition = null;

    if (statuses) {
      condition = { status: In(statuses) };
    } else {
      condition = { status: Object.values(STATUSES)[STATUSES.ACTIVE] };
    }

    const restaurant = await this.restaurantRepository.findOne({
      where: {
        id: userId,
        ...condition,
      },
    });

    if (!restaurant) throw new UnauthorizedException('error.restaurant not found');

    return restaurant;
  }

  // create(createRestaurantDto: CreateRestaurantDto) {
  //   console.log(createRestaurantDto);
  //   return 'This action adds a new restaurant';
  // }

  async update(id: number, updateRestaurantDto: UpdateRestaurantDto) {
    if (!id) throw new BadRequestException(`Update error: id is empty.`);

    try {
      updateRestaurantDto.id = id;

      if (updateRestaurantDto.password) {
        updateRestaurantDto.password = await passwordMd5Encrypt(updateRestaurantDto.password);
      }

      return this.restaurantRepository.save(updateRestaurantDto);
    } catch (ex) {
      throw new BadRequestException(`Update error: ${ex.message}`);
    }
  }
}
