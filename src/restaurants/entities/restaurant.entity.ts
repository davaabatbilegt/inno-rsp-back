import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Exclude, Expose } from 'class-transformer';

import { STATUSES } from './../../utils/constants';

import { RestaurantMenu } from './../../restaurant-menus/entities/restaurant-menu.entity';
import { RestaurantOrder } from './../../restaurant-orders/entities/restaurant-order.entity';
import { RestaurantOrderDetail } from './../../restaurant-order-details/entities/restaurant-order-detail.entity';
import { RestaurantOrganizationContract } from './../../restaurant-organization-contracts/entities/restaurant-organization-contract.entity';

@Entity({ name: 'restaurants' })
export class Restaurant {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column()
  name: string;

  @Column()
  address: string;

  @Column()
  description: string;

  @Column('decimal', { precision: 4, scale: 2 })
  rating: number;

  @Column('decimal', { precision: 13, scale: 2 })
  latitude: number;

  @Column('decimal', { precision: 13, scale: 2 })
  longitude: number;

  @Column()
  capacity: number;

  @Column({
    type: 'enum',
    enum: STATUSES,
    default: STATUSES.ACTIVE,
    transformer: {
      to: (value: STATUSES) => STATUSES[value],
      from: (value: number) => STATUSES[value],
    },
  })
  status: string;

  @Column({ unique: true })
  username: string;

  @Exclude()
  @Column()
  password: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @Expose()
  contracted?: number;

  @OneToMany(() => RestaurantOrganizationContract, (contract) => contract.restaurant)
  contracts: RestaurantOrganizationContract[];

  @OneToMany(() => RestaurantOrder, (order) => order.restaurant)
  orders: RestaurantOrder[];

  @OneToMany(() => RestaurantOrderDetail, (orderDetail) => orderDetail.restaurant)
  orderDetails: RestaurantOrderDetail[];

  @OneToMany(() => RestaurantMenu, (menu) => menu.restaurant)
  menus: RestaurantMenu[];

  constructor(partial: Partial<Restaurant>) {
    Object.assign(this, partial);
  }
}
