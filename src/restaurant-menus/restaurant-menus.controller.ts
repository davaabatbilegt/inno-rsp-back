import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Query,
  Request,
  UseInterceptors,
  ClassSerializerInterceptor,
} from '@nestjs/common';
import { ApiBody, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';

import {
  AdminAccess,
  EmployeeAccess,
  OrganizationAccess,
  RestaurantAccess,
  UserAccess,
} from './../decorators/guard.decorator';
import {
  validateMenuDays,
  validatePagination,
  validateSortOrder,
  validateStatus,
} from './../utils/functions';
import { MENU_DAYS, SORT_ORDER, STATUSES } from './../utils/constants';

import { RestaurantMenusService } from './restaurant-menus.service';
import { CreateRestaurantMenuDto } from './dto/create-restaurant-menu.dto';
import { UpdateRestaurantMenuDto } from './dto/update-restaurant-menu.dto';
import { RestaurantMenu } from './entities/restaurant-menu.entity';

const prefix = 'restaurant-menus';

@ApiTags(prefix)
@ApiResponse({ status: 401, description: 'Unauthorized.' })
@UseInterceptors(ClassSerializerInterceptor)
@Controller(prefix)
export class RestaurantMenusController {
  constructor(private readonly restaurantMenusService: RestaurantMenusService) {}

  @RestaurantAccess()
  @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
  @ApiBody({
    type: CreateRestaurantMenuDto,
    description: 'Json structure for restaurant menu object',
  })
  @Post()
  create(@Request() req, @Body() createRestaurantMenuDto: CreateRestaurantMenuDto) {
    return this.restaurantMenusService.create(createRestaurantMenuDto, req.user.id, req.user.type);
  }

  @AdminAccess()
  @EmployeeAccess()
  @RestaurantAccess()
  @OrganizationAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @ApiQuery({
    name: 'restaurantId',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'status',
    required: false,
    type: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'menuDays',
    required: false,
    type: `${Object.values(MENU_DAYS).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'contracted',
    required: false,
    type: Boolean,
  })
  @ApiQuery({
    name: 'page',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: `${Object.values(SORT_ORDER).filter((v) => isNaN(Number(v)))}`,
  })
  @Get()
  findAll(
    @Request() req,
    @Query('restaurantId') restaurantId: string,
    @Query('menuDays') menuDays: string | MENU_DAYS,
    @Query('status') status: string | STATUSES,
    @Query('contracted') contracted: boolean,
    @Query() options: IPaginationOptions,
    @Query('sortOrder') sortOrder: SORT_ORDER,
  ): Promise<Pagination<RestaurantMenu>> {
    validatePagination(options, prefix);
    sortOrder = validateSortOrder(sortOrder);
    status = validateStatus(status);
    menuDays = validateMenuDays(menuDays);

    return this.restaurantMenusService.findAll(
      restaurantId,
      status,
      menuDays,
      contracted,
      options,
      sortOrder,
      req.user.id,
      req.user.type,
    );
  }

  @AdminAccess()
  @EmployeeAccess()
  @RestaurantAccess()
  @OrganizationAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @Get(':id')
  findOne(@Request() req, @Param('id') id: string) {
    return this.restaurantMenusService.findOne(+id, req.user.id, req.user.type);
  }

  @RestaurantAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully updated.' })
  @ApiBody({
    type: UpdateRestaurantMenuDto,
    description: 'Json structure for restaurant menu object',
  })
  @Patch(':id')
  update(
    @Request() req,
    @Param('id') id: string,
    @Body() updateRestaurantMenuDto: UpdateRestaurantMenuDto,
  ) {
    return this.restaurantMenusService.update(
      +id,
      updateRestaurantMenuDto,
      req.user.id,
      req.user.type,
    );
  }
}
