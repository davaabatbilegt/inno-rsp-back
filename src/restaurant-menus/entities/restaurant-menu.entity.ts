import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Expose } from 'class-transformer';

import { MENU_DAYS, STATUSES } from './../../utils/constants';

import { Restaurant } from './../../restaurants/entities/restaurant.entity';
import { RestaurantMenuType } from './../../restaurant-menu-types/entities/restaurant-menu-type.entity';
import { RestaurantOrderDetail } from './../../restaurant-order-details/entities/restaurant-order-detail.entity';
import { RestaurantOrganizationContract } from './../../restaurant-organization-contracts/entities/restaurant-organization-contract.entity';
import { RestaurantInvoiceDetail } from './../../restaurant-invoice-details/entities/restaurant-invoice-detail.entity';

@Entity({ name: 'restaurant_menus' })
export class RestaurantMenu {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ name: 'restaurant_id' })
  restaurantId: number;

  @Column({ name: 'restaurant_menu_type_id' })
  restaurantMenuTypeId: number;

  @Column()
  name: string;

  @Column({ name: 'external_menu_id' })
  externalMenuId: number;

  @Column({
    type: 'enum',
    enum: STATUSES,
    default: STATUSES.ACTIVE,
    transformer: {
      to: (value: STATUSES) => STATUSES[value],
      from: (value: number) => STATUSES[value],
    },
  })
  status: string;

  @Column()
  description: string;

  @Column()
  price: number;

  @Column({
    type: 'enum',
    enum: MENU_DAYS,
    default: MENU_DAYS.ALL,
    transformer: {
      to: (value: MENU_DAYS) => MENU_DAYS[value],
      from: (value: number) => MENU_DAYS[value],
    },
  })
  days: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @Expose()
  contracted?: number;

  // Many to One cascade
  @ManyToOne(() => Restaurant, (restaurant) => restaurant.menus, { cascade: true, eager: true })
  @JoinColumn({ name: 'restaurant_id' })
  restaurant: Restaurant;

  @ManyToOne(() => RestaurantMenuType, (menuType) => menuType.menus, { cascade: true, eager: true })
  @JoinColumn({ name: 'restaurant_menu_type_id' })
  menuType: RestaurantMenuType;

  // One to Many cascade
  @OneToMany(() => RestaurantInvoiceDetail, (invoiceDetail) => invoiceDetail.menu)
  invoiceDetail: RestaurantInvoiceDetail;

  @OneToMany(() => RestaurantOrderDetail, (orderDetail) => orderDetail.menu)
  orderDetail: RestaurantOrderDetail;

  @OneToMany(() => RestaurantOrganizationContract, (order) => order.menu)
  contracts: RestaurantOrganizationContract[];

  constructor(partial: Partial<RestaurantMenu>) {
    Object.assign(this, partial);
  }
}
