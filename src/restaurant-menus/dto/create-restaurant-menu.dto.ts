import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

import { MENU_DAYS, STATUSES } from './../../utils/constants';

export class CreateRestaurantMenuDto {
  @ApiProperty({
    example: 1,
    required: true,
  })
  restaurantId: number;

  @ApiProperty({
    example: 1,
    required: true,
  })
  restaurantMenuTypeId: number;

  @ApiProperty({
    example: 'tesMenuName',
    required: true,
  })
  @IsNotEmpty({ message: 'Please enter menu name' })
  name: string;

  @ApiProperty({
    example: 1,
    required: false,
  })
  externalMenuId: number;

  @ApiProperty({
    enum: STATUSES,
    example: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
    required: false,
  })
  status: string;

  @ApiProperty({
    example: 'this food made from natural sources',
    required: false,
  })
  description: string;

  @ApiProperty({
    example: 10000,
    required: true,
  })
  price: number;

  @ApiProperty({
    enum: MENU_DAYS,
    example: `${Object.values(MENU_DAYS).filter((v) => isNaN(Number(v)))}`,
    required: false,
  })
  days: string;
}
