import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { RestaurantMenusService } from './restaurant-menus.service';
import { RestaurantMenusController } from './restaurant-menus.controller';
import { RestaurantMenu } from './entities/restaurant-menu.entity';

import { OrganizationEmployeesModule } from './../organization-employees/organization-employees.module';

@Module({
  imports: [OrganizationEmployeesModule, TypeOrmModule.forFeature([RestaurantMenu])],
  controllers: [RestaurantMenusController],
  providers: [RestaurantMenusService],
  exports: [RestaurantMenusService],
})
export class RestaurantMenusModule {}
