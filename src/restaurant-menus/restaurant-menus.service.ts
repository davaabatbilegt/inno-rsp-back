import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Brackets, Repository } from 'typeorm';
import { IPaginationOptions, Pagination, paginate, paginateRaw } from 'nestjs-typeorm-paginate';

import { MENU_DAYS, SORT_ORDER, STATUSES, USER_TYPES } from './../utils/constants';
import { restaurantCondition } from './../utils/functions';

import { CreateRestaurantMenuDto } from './dto/create-restaurant-menu.dto';
import { UpdateRestaurantMenuDto } from './dto/update-restaurant-menu.dto';
import { RestaurantMenu } from './entities/restaurant-menu.entity';

import { OrganizationEmployeesService } from './../organization-employees/organization-employees.service';

@Injectable()
export class RestaurantMenusService {
  constructor(
    @InjectRepository(RestaurantMenu)
    private restaurantMenuRepository: Repository<RestaurantMenu>,
    private organizationEmployeesService: OrganizationEmployeesService,
  ) {}

  create(
    createRestaurantMenuDto: CreateRestaurantMenuDto,
    userId: string,
    type: string,
  ): Promise<RestaurantMenu | undefined> {
    try {
      if (type == USER_TYPES.RESTAURANT) {
        createRestaurantMenuDto.restaurantId = parseInt(userId, 10);
      }
      return this.restaurantMenuRepository.save(createRestaurantMenuDto);
    } catch (ex) {
      throw new BadRequestException(`Create error: ${ex.message}`);
    }
  }

  async findAll(
    restaurantId: string,
    status: string | STATUSES,
    menuDays: string | MENU_DAYS,
    contracted: boolean,
    options: IPaginationOptions,
    sortOrder: SORT_ORDER,
    userId: string,
    type: string,
  ): Promise<Pagination<RestaurantMenu>> {
    const queryBuilder = this.restaurantMenuRepository.createQueryBuilder('rm');

    if (type == USER_TYPES.RESTAURANT) {
      queryBuilder.andWhere({ restaurantId: userId });
    } else if (restaurantId) {
      queryBuilder.andWhere({ restaurantId: restaurantId });
    }

    if (status) {
      queryBuilder.andWhere({ status: status });
    }

    if (menuDays) {
      queryBuilder.andWhere(
        new Brackets((qb) => {
          qb.andWhere({ days: menuDays }).orWhere({
            days: Object.values(MENU_DAYS)[MENU_DAYS.ALL],
          });
        }),
      );
    }

    let organizationId: number;
    if (type == USER_TYPES.ORGANIZATION) {
      organizationId = +userId;
    } else if (type == USER_TYPES.EMPLOYEE) {
      const employee = await this.organizationEmployeesService.findOneById(userId);
      organizationId = employee.organizationId;
    }

    queryBuilder
      .leftJoinAndSelect('rm.restaurant', 'r')
      .leftJoinAndSelect('rm.menuType', 'rmt')
      .orderBy('rm.name', sortOrder)
      .getRawMany();

    if (type == USER_TYPES.ORGANIZATION || type == USER_TYPES.EMPLOYEE) {
      queryBuilder
        .leftJoin('rm.contracts', 'roc')
        .addSelect(
          `SUM(IF(roc.organization_id = ${organizationId} AND roc.status = 1 AND roc.restaurant_status = 1 AND roc.organization_status = 1, 1, 0))`,
          'contracted',
        )
        .andWhere('roc.organizationId = :organizationId', { organizationId: organizationId })
        .groupBy('rm.id');

      if (contracted != undefined) {
        if (contracted) {
          queryBuilder.having('contracted > 0');
        } else {
          queryBuilder.having('contracted = 0');
        }
      }

      const raw = await paginateRaw<RestaurantMenu>(queryBuilder, options);
      const entities = await paginate<RestaurantMenu>(queryBuilder, options);

      for (let i = 0; i < entities.items.length; i++) {
        entities.items[i].contracted = raw.items[i].contracted;
      }
      return entities;
    } else {
      return paginate<RestaurantMenu>(queryBuilder, options);
    }
  }

  async findOne(id: number, userId: string, type: string): Promise<RestaurantMenu> {
    let condition = null;
    condition = restaurantCondition(userId, type);

    const queryBuilder = this.restaurantMenuRepository.createQueryBuilder('rm');
    queryBuilder.leftJoinAndSelect('rm.restaurant', 'r').leftJoinAndSelect('rm.menuType', 'rmt');

    if (type == USER_TYPES.ORGANIZATION) {
      queryBuilder
        .leftJoin('rm.contracts', 'roc')
        .addSelect(
          `SUM(IF(roc.organization_id = ${userId} AND roc.status = 1 AND roc.restaurant_status = 1 AND roc.organization_status = 1, 1, 0))`,
          'contracted',
        )
        .andWhere({ id: id });
    } else {
      queryBuilder.andWhere({ id: id });
    }

    if (condition) {
      queryBuilder.andWhere(condition);
    }

    const restaurantMenu = await queryBuilder.getOne();

    if (!restaurantMenu) throw new UnauthorizedException('error.restaurantMenu not found');

    if (type == USER_TYPES.ORGANIZATION) {
      const restaurantMenuRaw = await queryBuilder.getRawOne();
      restaurantMenu.contracted = restaurantMenuRaw['contracted'];
    }

    return restaurantMenu;
  }

  async update(
    id: number,
    updateRestaurantMenuDto: UpdateRestaurantMenuDto,
    userId: string,
    type: string,
  ): Promise<RestaurantMenu | undefined> {
    if (!id) throw new BadRequestException(`Update error: id is empty.`);

    try {
      // Menu should belongs logged user
      const targetMenu = await this.findOne(id, userId, type);
      if (!targetMenu) {
        throw new BadRequestException(`Update error: menu not belongs to restaurant`);
      }

      updateRestaurantMenuDto.id = id;

      return this.restaurantMenuRepository.save(updateRestaurantMenuDto);
    } catch (ex) {
      throw new BadRequestException(`Update error: ${ex.message}`);
    }
  }

  checkMenuBelongsToRestaurant(id: number, restaurantId: number): boolean {
    try {
      const menu = this.restaurantMenuRepository.findOne({
        where: {
          id: id,
          restaurantId: restaurantId,
        },
      });
      if (menu) {
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      throw new BadRequestException(`checkMenuBelonsToRestaurant error: ${ex.message}`);
    }
  }
}
