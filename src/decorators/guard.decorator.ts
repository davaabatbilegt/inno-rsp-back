import { SetMetadata } from '@nestjs/common';

import { IS_PUBLIC_KEY, USER_TYPES } from '../utils/constants';

export const Public = () => SetMetadata(IS_PUBLIC_KEY, true);

export const AdminAccess = () => SetMetadata(USER_TYPES.ADMIN, true);

export const RestaurantAccess = () => SetMetadata(USER_TYPES.RESTAURANT, true);

export const OrganizationAccess = () => SetMetadata(USER_TYPES.ORGANIZATION, true);

export const EmployeeAccess = () => SetMetadata(USER_TYPES.EMPLOYEE, true);

export const UserAccess = () => SetMetadata(USER_TYPES.USER, true);
