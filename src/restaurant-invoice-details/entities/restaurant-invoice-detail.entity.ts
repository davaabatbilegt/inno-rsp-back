import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { STATUSES } from './../../utils/constants';

import { Restaurant } from './../../restaurants/entities/restaurant.entity';
import { Organization } from './../../organizations/entities/organization.entity';
import { RestaurantMenu } from './../../restaurant-menus/entities/restaurant-menu.entity';
import { RestaurantInvoice } from './../../restaurant-invoices/entities/restaurant-invoice.entity';

@Entity({ name: 'restaurant_invoice_details' })
export class RestaurantInvoiceDetail {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ name: 'restaurant_id' })
  restaurantId: number;

  @Column({ name: 'restaurant_invoice_id' })
  restaurantInvoiceId: number;

  @Column({ name: 'organization_id' })
  organizationId: number;

  @Column({ name: 'year_month' })
  yearMonth: Date;

  @Column({ name: 'restaurant_menu_id' })
  restaurantMenuId: number;

  @Column({
    type: 'enum',
    enum: STATUSES,
    default: STATUSES.ACTIVE,
    transformer: {
      to: (value: STATUSES) => STATUSES[value],
      from: (value: number) => STATUSES[value],
    },
  })
  status: string;

  @Column({ name: 'unit_count' })
  unitCount: number;

  @Column({ name: 'unit_price' })
  unitPrice: number;

  @Column({ name: 'total_price' })
  totalPrice: number;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  // Many to One cascade
  @ManyToOne(() => Restaurant, (restaurant) => restaurant.contracts, { cascade: true, eager: true })
  @JoinColumn({ name: 'restaurant_id' })
  restaurant: Restaurant;

  @ManyToOne(() => Organization, (organization) => organization.contracts, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: 'organization_id' })
  organization: Organization;

  // Many to One cascade
  @ManyToOne(() => RestaurantInvoice, (restaurantInvoice) => restaurantInvoice.invoiceDetails, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: 'restaurant_invoice_id' })
  restaurantInvoice: RestaurantInvoice;

  @ManyToOne(() => RestaurantMenu, (menu) => menu.invoiceDetail, { cascade: true, eager: true })
  @JoinColumn({ name: 'restaurant_menu_id' })
  menu: RestaurantMenu;

  constructor(partial: Partial<RestaurantInvoiceDetail>) {
    Object.assign(this, partial);
  }
}
