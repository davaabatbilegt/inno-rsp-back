import {
  Controller,
  Get,
  Query,
  UseInterceptors,
  ClassSerializerInterceptor,
} from '@nestjs/common';
import { ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';

import { OrganizationAccess, RestaurantAccess } from './../decorators/guard.decorator';
import { validateSortOrder, validateStatus } from './../utils/functions';
import { SORT_ORDER, STATUSES } from './../utils/constants';

import { RestaurantInvoiceDetailsService } from './restaurant-invoice-details.service';
import { RestaurantInvoiceDetail } from './entities/restaurant-invoice-detail.entity';

const prefix = 'restaurant-invoice-details';

@ApiTags(prefix)
@ApiResponse({ status: 401, description: 'Unauthorized.' })
@UseInterceptors(ClassSerializerInterceptor)
@Controller(prefix)
export class RestaurantInvoiceDetailsController {
  constructor(private readonly restaurantInvoiceDetailsService: RestaurantInvoiceDetailsService) {}

  @RestaurantAccess()
  @OrganizationAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @ApiQuery({
    name: 'invoiceId',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'status',
    required: false,
    type: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: `${Object.values(SORT_ORDER).filter((v) => isNaN(Number(v)))}`,
  })
  @Get()
  findAll(
    @Query('invoiceId') invoiceId: string,
    @Query('status') status: string | STATUSES,
    @Query('sortOrder') sortOrder: SORT_ORDER,
  ): Promise<RestaurantInvoiceDetail[]> {
    sortOrder = validateSortOrder(sortOrder);
    status = validateStatus(status);

    return this.restaurantInvoiceDetailsService.findAllByInvoiceId(invoiceId, status, sortOrder);
  }
}
