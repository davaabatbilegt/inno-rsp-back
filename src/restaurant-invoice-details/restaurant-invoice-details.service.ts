import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { SORT_ORDER, STATUSES } from './../utils/constants';

import { CreateRestaurantInvoiceDetailDto } from './dto/create-restaurant-invoice-detail.dto';
import { RestaurantInvoiceDetail } from './entities/restaurant-invoice-detail.entity';

@Injectable()
export class RestaurantInvoiceDetailsService {
  constructor(
    @InjectRepository(RestaurantInvoiceDetail)
    private restaurantInvoiceDetailRepository: Repository<RestaurantInvoiceDetail>,
  ) {}

  async createRestaurantInvoiceDetail(
    createRestaurantInvoiceDetailDto: CreateRestaurantInvoiceDetailDto,
  ): Promise<RestaurantInvoiceDetail | undefined> {
    try {
      return this.restaurantInvoiceDetailRepository.save(createRestaurantInvoiceDetailDto);
    } catch (ex) {
      throw new BadRequestException(`Create error: ${ex.message}`);
    }
  }

  async findAllByInvoiceId(
    invoiceId: string,
    status: string | STATUSES,
    sortOrder: SORT_ORDER,
  ): Promise<RestaurantInvoiceDetail[]> {
    const queryBuilder = this.restaurantInvoiceDetailRepository.createQueryBuilder('rid');

    if (invoiceId) {
      queryBuilder.andWhere({ restaurantInvoiceId: invoiceId });
    }

    if (status) {
      queryBuilder.andWhere({ status: status }).andWhere('ri.status = :status', { status: status });
    }

    queryBuilder
      .leftJoinAndSelect('rid.restaurantInvoice', 'ri')
      .leftJoinAndSelect('rid.restaurant', 'r')
      .leftJoinAndSelect('rid.organization', 'o')
      .leftJoinAndSelect('rid.menu', 'rm')
      .orderBy(`rid.yearMonth`, sortOrder);

    return queryBuilder.getMany();
  }

  async findAll(
    restaurantId: number,
    organizationId: number,
    yearMonth: Date,
    restaurantMenuId?: number,
  ): Promise<RestaurantInvoiceDetail[]> {
    let condition = null;
    if (restaurantMenuId) {
      condition = { restaurantMenuId: restaurantMenuId };
    }
    return this.restaurantInvoiceDetailRepository.find({
      where: {
        restaurantId: restaurantId,
        organizationId: organizationId,
        yearMonth: yearMonth,
        status: Object.values(STATUSES)[STATUSES.ACTIVE],
        ...condition,
      },
    });
  }
}
