export class CreateRestaurantInvoiceDetailDto {
  restaurantInvoiceId: number;

  restaurantId: number;

  organizationId: number;

  yearMonth: Date;

  restaurantMenuId: number;

  status: string;

  unitCount: number;

  unitPrice: number;

  totalPrice: number;
}
