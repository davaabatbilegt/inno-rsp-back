import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { RestaurantInvoiceDetailsService } from './restaurant-invoice-details.service';
import { RestaurantInvoiceDetailsController } from './restaurant-invoice-details.controller';
import { RestaurantInvoiceDetail } from './entities/restaurant-invoice-detail.entity';

@Module({
  imports: [TypeOrmModule.forFeature([RestaurantInvoiceDetail])],
  controllers: [RestaurantInvoiceDetailsController],
  providers: [RestaurantInvoiceDetailsService],
  exports: [RestaurantInvoiceDetailsService],
})
export class RestaurantInvoiceDetailsModule {}
