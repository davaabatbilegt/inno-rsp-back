import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';

import { STATUSES } from './../../utils/constants';

@Entity({ name: 'admins' })
export class Admin {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column()
  name: string;

  @Column({
    type: 'enum',
    enum: STATUSES,
    default: STATUSES.ACTIVE,
    transformer: {
      to: (value: STATUSES) => STATUSES[value],
      from: (value: number) => STATUSES[value],
    },
  })
  status: string;

  @Column({ unique: true })
  username: string;

  @Exclude()
  @Column()
  password: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  constructor(partial: Partial<Admin>) {
    Object.assign(this, partial);
  }
}
