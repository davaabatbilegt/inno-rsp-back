import { Controller } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { AdminsService } from './admins.service';
// import { CreateAdminDto } from './dto/create-admin.dto';
// import { UpdateAdminDto } from './dto/update-admin.dto';

@ApiTags('admins')
@ApiResponse({ status: 401, description: 'Unauthorized.' })
@Controller('admins')
export class AdminsController {
  constructor(private readonly adminsService: AdminsService) {}

  // @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
  // @ApiBody({
  //   type: CreateAdminDto,
  //   description: 'Json structure for admin object',
  // })
  // @Post()
  // create(@Body() createAdminDto: CreateAdminDto) {
  //   return this.adminsService.create(createAdminDto);
  // }

  // @ApiResponse({ status: 200, description: 'The records has been successfully fetched.' })
  // @Get()
  // findAll() {
  //   return this.adminsService.findAll();
  // }

  // @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.adminsService.findOne(+id);
  // }

  // @ApiResponse({ status: 200, description: 'The record has been successfully updated.' })
  // @ApiBody({
  //   type: UpdateAdminDto,
  //   description: 'Json structure for organization employee object',
  // })
  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateAdminDto: UpdateAdminDto) {
  //   return this.adminsService.update(+id, updateAdminDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.adminsService.remove(+id);
  // }
}
