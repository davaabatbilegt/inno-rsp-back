import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';

import { STATUSES } from './../utils/constants';

import { CreateAdminDto } from './dto/create-admin.dto';
import { UpdateAdminDto } from './dto/update-admin.dto';
import { Admin } from './entities/admin.entity';

@Injectable()
export class AdminsService {
  constructor(@InjectRepository(Admin) private adminRepository: Repository<Admin>) {}

  async findOneByUsername(username: string, statuses?: STATUSES[]): Promise<Admin> {
    let condition = null;

    if (statuses) {
      condition = { status: In(statuses) };
    } else {
      condition = { status: Object.values(STATUSES)[STATUSES.ACTIVE] };
    }

    const admin = await this.adminRepository.findOne({
      where: {
        username: username,
        ...condition,
      },
    });

    if (!admin) throw new UnauthorizedException('error.admin not found');

    return admin;
  }

  async findOneById(userId: string, statuses?: STATUSES[]): Promise<Admin> {
    let condition = null;

    if (statuses) {
      condition = { status: In(statuses) };
    } else {
      condition = { status: Object.values(STATUSES)[STATUSES.ACTIVE] };
    }

    const admin = await this.adminRepository.findOne({
      where: {
        id: userId,
        ...condition,
      },
    });

    if (!admin) throw new UnauthorizedException('error.admin not found');

    return admin;
  }

  create(createAdminDto: CreateAdminDto) {
    console.log(createAdminDto);
    return 'This action adds a new admin';
  }

  findAll() {
    return `This action returns all admins`;
  }

  findOne(id: number) {
    return `This action returns a #${id} admin`;
  }

  update(id: number, updateAdminDto: UpdateAdminDto) {
    console.log(updateAdminDto);
    return `This action updates a #${id} admin`;
  }

  remove(id: number) {
    return `This action removes a #${id} admin`;
  }
}
