import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { CONTRACT_STATUSES, STATUSES } from './../../utils/constants';

import { Restaurant } from './../../restaurants/entities/restaurant.entity';
import { Organization } from './../../organizations/entities/organization.entity';
import { RestaurantMenu } from './../../restaurant-menus/entities/restaurant-menu.entity';

@Entity({ name: 'restaurant_organization_contracts' })
export class RestaurantOrganizationContract {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ name: 'restaurant_id' })
  restaurantId: number;

  @Column({ name: 'organization_id' })
  organizationId: number;

  @Column({
    name: 'restaurant_status',
    type: 'enum',
    enum: CONTRACT_STATUSES,
    default: CONTRACT_STATUSES.PENDING,
    transformer: {
      to: (value: CONTRACT_STATUSES) => CONTRACT_STATUSES[value],
      from: (value: number) => CONTRACT_STATUSES[value],
    },
  })
  restaurantStatus: string;

  @Column({
    name: 'organization_status',
    type: 'enum',
    enum: CONTRACT_STATUSES,
    default: CONTRACT_STATUSES.PENDING,
    transformer: {
      to: (value: CONTRACT_STATUSES) => CONTRACT_STATUSES[value],
      from: (value: number) => CONTRACT_STATUSES[value],
    },
  })
  organizationStatus: string;

  @Column({ name: 'menu_id' })
  menuId: number;

  @Column({ name: 'menu_unit_price' })
  menuUnitPrice: number;

  @Column({
    type: 'enum',
    enum: STATUSES,
    default: STATUSES.ACTIVE,
    transformer: {
      to: (value: STATUSES) => STATUSES[value],
      from: (value: number) => STATUSES[value],
    },
  })
  status: string;

  @Column('time', { name: 'order_limit_time' })
  orderLimitTime: Date;

  @Column({ name: 'order_limit_food_count' })
  orderLimitFoodCount: number;

  @Column({ name: 'order_limit_food_money' })
  orderLimitFoodMoney: number;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @ManyToOne(() => Restaurant, (restaurant) => restaurant.contracts, { cascade: true, eager: true })
  @JoinColumn({ name: 'restaurant_id' })
  restaurant: Restaurant;

  @ManyToOne(() => Organization, (organization) => organization.contracts, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: 'organization_id' })
  organization: Organization;

  @ManyToOne(() => RestaurantMenu, (menu) => menu.contracts, { cascade: true, eager: true })
  @JoinColumn({ name: 'menu_id' })
  menu: RestaurantMenu;

  constructor(partial: Partial<RestaurantOrganizationContract>) {
    Object.assign(this, partial);
  }
}
