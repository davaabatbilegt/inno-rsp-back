import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { RestaurantOrganizationContractsService } from './restaurant-organization-contracts.service';
import { RestaurantOrganizationContractsController } from './restaurant-organization-contracts.controller';
import { RestaurantOrganizationContract } from './entities/restaurant-organization-contract.entity';

import { RestaurantMenusModule } from './../restaurant-menus/restaurant-menus.module';
import { OrganizationEmployee } from './../organization-employees/entities/organization-employee.entity';

@Module({
  imports: [
    forwardRef(() => RestaurantMenusModule),
    TypeOrmModule.forFeature([RestaurantOrganizationContract, OrganizationEmployee]),
  ],
  controllers: [RestaurantOrganizationContractsController],
  providers: [RestaurantOrganizationContractsService],
  exports: [RestaurantOrganizationContractsService],
})
export class RestaurantOrganizationContractsModule {}
