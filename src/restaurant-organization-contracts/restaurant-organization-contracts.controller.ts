import {
  Controller,
  Request,
  Get,
  Post,
  Body,
  Patch,
  Param,
  UseInterceptors,
  ClassSerializerInterceptor,
  Query,
} from '@nestjs/common';
import { ApiBody, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';

import {
  EmployeeAccess,
  OrganizationAccess,
  RestaurantAccess,
} from './../decorators/guard.decorator';
import {
  validateMenuDays,
  validatePagination,
  validateSortOrder,
  validateStatus,
} from './../utils/functions';
import { CONTRACT_STATUSES, MENU_DAYS, SORT_ORDER, STATUSES } from './../utils/constants';

import { RestaurantOrganizationContractsService } from './restaurant-organization-contracts.service';

import { CreateRestaurantOrganizationContractDto } from './dto/create-restaurant-organization-contract.dto';
import { UpdateRestaurantOrganizationContractDto } from './dto/update-restaurant-organization-contract.dto';
import { RestaurantOrganizationContract } from './entities/restaurant-organization-contract.entity';

const prefix = 'restaurant-organization-contracts';

@ApiTags(prefix)
@ApiResponse({ status: 401, description: 'Unauthorized.' })
@UseInterceptors(ClassSerializerInterceptor)
@Controller(prefix)
export class RestaurantOrganizationContractsController {
  constructor(
    private readonly restaurantOrganizationContractsService: RestaurantOrganizationContractsService,
  ) {}

  @RestaurantAccess()
  @OrganizationAccess()
  @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
  @ApiBody({
    type: CreateRestaurantOrganizationContractDto,
    description: 'Json structure for organization employee object',
  })
  @Post()
  create(
    @Request() req,
    @Body() createRestaurantOrganizationContractDto: CreateRestaurantOrganizationContractDto,
  ) {
    return this.restaurantOrganizationContractsService.create(
      createRestaurantOrganizationContractDto,
      req.user.id,
      req.user.type,
    );
  }

  @RestaurantAccess()
  @OrganizationAccess()
  @EmployeeAccess()
  @ApiResponse({ status: 200, description: 'The records has been successfully fetched.' })
  @ApiQuery({
    name: 'restaurantId',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'organizationId',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'restaurantContractStatus',
    required: false,
    type: `${Object.values(CONTRACT_STATUSES).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'organizationContractStatus',
    required: false,
    type: `${Object.values(CONTRACT_STATUSES).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'menuDays',
    required: false,
    type: `${Object.values(MENU_DAYS).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'status',
    required: false,
    type: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'page',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: `${Object.values(SORT_ORDER).filter((v) => isNaN(Number(v)))}`,
  })
  @Get()
  findAll(
    @Request() req,
    @Query('restaurantId') restaurantId: number,
    @Query('organizationId') organizationId: number,
    @Query('restaurantContractStatus') restaurantContractStatus: string | CONTRACT_STATUSES,
    @Query('organizationContractStatus') organizationContractStatus: string | CONTRACT_STATUSES,
    @Query('menuDays') menuDays: string | MENU_DAYS,
    @Query('status') status: string | STATUSES,
    @Query() options: IPaginationOptions,
    @Query('sortOrder') sortOrder: SORT_ORDER,
  ): Promise<Pagination<RestaurantOrganizationContract>> {
    validatePagination(options, prefix);
    sortOrder = validateSortOrder(sortOrder);
    status = validateStatus(status);
    menuDays = validateMenuDays(menuDays);

    return this.restaurantOrganizationContractsService.findAll(
      restaurantId,
      organizationId,
      restaurantContractStatus,
      organizationContractStatus,
      menuDays,
      status,
      options,
      sortOrder,
      req.user.id,
      req.user.type,
    );
  }

  @RestaurantAccess()
  @OrganizationAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @Get(':id')
  findOne(@Request() req, @Param('id') id: string) {
    return this.restaurantOrganizationContractsService.findOne(+id, req.user.id, req.user.type);
  }

  @RestaurantAccess()
  @OrganizationAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully updated.' })
  @ApiBody({
    type: UpdateRestaurantOrganizationContractDto,
    description: 'Json structure for organization employee object',
  })
  @Patch(':id')
  update(
    @Request() req,
    @Param('id') id: string,
    @Body() updateRestaurantOrganizationContractDto: UpdateRestaurantOrganizationContractDto,
  ) {
    return this.restaurantOrganizationContractsService.update(
      +id,
      updateRestaurantOrganizationContractDto,
      req.user.id,
      req.user.type,
    );
  }
}
