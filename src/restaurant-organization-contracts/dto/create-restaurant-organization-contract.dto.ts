import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

import { CONTRACT_STATUSES, STATUSES } from './../../utils/constants';

export class CreateRestaurantOrganizationContractDto {
  @ApiProperty({
    example: 1,
    required: true,
  })
  restaurantId: number;

  @ApiProperty({
    enum: CONTRACT_STATUSES,
    example: `${Object.values(CONTRACT_STATUSES).filter((v) => isNaN(Number(v)))}`,
    required: false,
  })
  restaurantStatus: string;

  @ApiProperty({
    example: 1,
    required: true,
  })
  organizationId: number;

  @ApiProperty({
    enum: CONTRACT_STATUSES,
    example: `${Object.values(CONTRACT_STATUSES).filter((v) => isNaN(Number(v)))}`,
    required: false,
  })
  organizationStatus: string;

  @ApiProperty({
    example: 1,
    required: true,
  })
  @IsNotEmpty({ message: 'Please enter menu' })
  menuId: number;

  @ApiProperty({
    example: 15000,
    required: false,
  })
  menuUnitPrice: number;

  @ApiProperty({
    enum: STATUSES,
    example: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
    required: false,
  })
  status: string;

  @ApiProperty({
    example: '10:30:00',
    required: false,
  })
  orderLimitTime: Date;

  @ApiProperty({
    example: 20,
    required: false,
  })
  orderLimitFoodCount: number;

  @ApiProperty({
    example: 30000,
    required: false,
  })
  orderLimitFoodMoney: number;
}
