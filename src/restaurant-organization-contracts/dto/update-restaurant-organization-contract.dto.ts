import { PartialType } from '@nestjs/swagger';
import { CreateRestaurantOrganizationContractDto } from './create-restaurant-organization-contract.dto';

export class UpdateRestaurantOrganizationContractDto extends PartialType(
  CreateRestaurantOrganizationContractDto,
) {
  id: number;
}
