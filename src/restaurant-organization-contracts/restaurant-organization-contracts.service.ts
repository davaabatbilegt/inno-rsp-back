import {
  BadRequestException,
  Inject,
  Injectable,
  UnauthorizedException,
  forwardRef,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Brackets, Repository } from 'typeorm';
import { IPaginationOptions, Pagination, paginate } from 'nestjs-typeorm-paginate';

import { allUserTypeCondition } from './../utils/functions';
import {
  CONTRACT_STATUSES,
  MENU_DAYS,
  SORT_ORDER,
  STATUSES,
  USER_TYPES,
} from './../utils/constants';

import { CreateRestaurantOrganizationContractDto } from './dto/create-restaurant-organization-contract.dto';
import { UpdateRestaurantOrganizationContractDto } from './dto/update-restaurant-organization-contract.dto';
import { RestaurantOrganizationContract } from './entities/restaurant-organization-contract.entity';

import { RestaurantMenusService } from './../restaurant-menus/restaurant-menus.service';
import { OrganizationEmployeesService } from 'src/organization-employees/organization-employees.service';
import { OrganizationEmployee } from 'src/organization-employees/entities/organization-employee.entity';

@Injectable()
export class RestaurantOrganizationContractsService {
  constructor(
    @InjectRepository(RestaurantOrganizationContract)
    private contactRepository: Repository<RestaurantOrganizationContract>,
    @Inject(forwardRef(() => RestaurantMenusService))
    private restaurantMenuService: RestaurantMenusService,
    @InjectRepository(OrganizationEmployee)
    private employeeService: OrganizationEmployeesService,
  ) {}

  async create(
    createContractDto: CreateRestaurantOrganizationContractDto,
    userId: string,
    type: string,
  ): Promise<RestaurantOrganizationContract | undefined> {
    try {
      // If logged user Restaurant then use own restaurantId, organization status should be Pending
      if (type == USER_TYPES.RESTAURANT) {
        createContractDto.restaurantId = parseInt(userId, 10);
        createContractDto.organizationStatus =
          Object.values(CONTRACT_STATUSES)[CONTRACT_STATUSES.PENDING].toString();
      }

      // If logged user Organization then own organizationId, restaurant status should be Pending
      if (type == USER_TYPES.ORGANIZATION) {
        createContractDto.organizationId = parseInt(userId, 10);
        createContractDto.restaurantStatus =
          Object.values(CONTRACT_STATUSES)[CONTRACT_STATUSES.PENDING].toString();
      }

      // Menu should belongs to restaurant
      const isMenuBelongsToRestaurant = this.restaurantMenuService.checkMenuBelongsToRestaurant(
        createContractDto.menuId,
        createContractDto.restaurantId,
      );
      if (!isMenuBelongsToRestaurant) {
        throw new BadRequestException(`Menu does not belongs to restaurant`);
      }

      return this.contactRepository.save(createContractDto);
    } catch (ex) {
      throw new BadRequestException(`Create error: ${ex.message}`);
    }
  }

  async findAll(
    restaurantId: number,
    organizationId: number,
    restaurantContractStatus: string | CONTRACT_STATUSES,
    organizationContractStatus: string | CONTRACT_STATUSES,
    menuDays: string | MENU_DAYS,
    status: string | STATUSES,
    options: IPaginationOptions,
    sortOrder: SORT_ORDER,
    userId: string,
    type: string,
  ): Promise<Pagination<RestaurantOrganizationContract>> {
    const queryBuilder = this.contactRepository.createQueryBuilder('roc');

    if (type == USER_TYPES.ORGANIZATION) {
      queryBuilder.andWhere({ organizationId: userId });
    } else if (type == USER_TYPES.RESTAURANT) {
      queryBuilder.andWhere({ restaurantId: userId });
    } else if (type == USER_TYPES.EMPLOYEE) {
      const employeeDetail = await this.employeeService.findOneById(userId);
      queryBuilder.andWhere({ organizationId: employeeDetail.organizationId });
    }

    if (restaurantId && type != USER_TYPES.RESTAURANT) {
      queryBuilder.andWhere({ restaurantId: restaurantId });
    }

    if (organizationId && type != USER_TYPES.ORGANIZATION) {
      queryBuilder.andWhere({ organizationId: organizationId });
    }

    if (organizationContractStatus) {
      queryBuilder.andWhere({ organizationStatus: organizationContractStatus });
    }

    if (restaurantContractStatus) {
      queryBuilder.andWhere({ restaurantStatus: restaurantContractStatus });
    }

    if (status) {
      queryBuilder.andWhere({ status: status });
    }

    if (menuDays) {
      queryBuilder.andWhere(
        new Brackets((qb) => {
          qb.andWhere('m.days = :days', { days: menuDays }).orWhere('m.days = :daysAll', {
            daysAll: Object.values(MENU_DAYS)[MENU_DAYS.ALL],
          });
        }),
      );
    }

    queryBuilder
      .leftJoinAndSelect('roc.restaurant', 'r')
      .leftJoinAndSelect('roc.organization', 'o')
      .leftJoinAndSelect('roc.menu', 'm')
      .andWhere('m.status = :status', { status: STATUSES.ACTIVE })
      .orderBy('roc.createdAt', sortOrder);

    return paginate<RestaurantOrganizationContract>(queryBuilder, options);
  }

  async findOne(id: number, userId: string, type: string): Promise<RestaurantOrganizationContract> {
    let condition = null;
    condition = allUserTypeCondition(userId, type);

    const contract = await this.contactRepository.findOne({
      where: {
        id: id,
        ...condition,
      },
    });

    if (!contract) throw new UnauthorizedException('error.contract not found');

    return contract;
  }

  async update(
    id: number,
    updateContractDto: UpdateRestaurantOrganizationContractDto,
    userId: string,
    type: string,
  ): Promise<RestaurantOrganizationContract | undefined> {
    if (!id) throw new BadRequestException(`Update error: id is empty.`);
    updateContractDto.id = id;

    try {
      // Contact should belongs logged user
      const targetContract = await this.findOne(id, userId, type);
      if (!targetContract) {
        throw new BadRequestException(`Update error: contract not belongs to user`);
      }

      // Menu should belongs to restaurant
      const isMenuBelongsToRestaurant = this.restaurantMenuService.checkMenuBelongsToRestaurant(
        updateContractDto.menuId,
        targetContract.restaurantId,
      );
      if (!isMenuBelongsToRestaurant) {
        throw new BadRequestException(`Menu does not belongs to restaurant`);
      }

      // check-iig tur comment out bolgoloo
      // const isChangeOtherPartyStatus = this.isChangeOtherPartyStatus(
      //   updateContractDto,
      //   targetContract,
      // );

      // // organization should not update restaurant status
      // if (type == USER_TYPES.ORGANIZATION) {
      //   if (isChangeOtherPartyStatus == false) {
      //     updateContractDto.restaurantStatus = targetContract.restaurantStatus;
      //   } else {
      //     updateContractDto.restaurantStatus =
      //     Object.values(CONTRACT_STATUSES)[CONTRACT_STATUSES.PENDING].toString();
      //   }
      // }
      // // restaurant should not update organization status
      // if (type == USER_TYPES.RESTAURANT) {
      //   if (isChangeOtherPartyStatus == false) {
      //     updateContractDto.organizationStatus = targetContract.organizationStatus;
      //   } else {
      //     updateContractDto.organizationStatus =
      //     Object.values(CONTRACT_STATUSES)[CONTRACT_STATUSES.PENDING].toString();
      //   }
      // }

      return this.contactRepository.save(updateContractDto);
    } catch (ex) {
      throw new BadRequestException(`Update error: ${ex.message}`);
    }
  }

  isChangeOtherPartyStatus(
    updateContractDto: UpdateRestaurantOrganizationContractDto,
    targetContract: RestaurantOrganizationContract,
  ): boolean {
    if (updateContractDto.menuId && updateContractDto.menuId != targetContract.menuId) return true;
    if (
      updateContractDto.menuUnitPrice &&
      updateContractDto.menuUnitPrice != targetContract.menuUnitPrice
    )
      return true;
    if (
      updateContractDto.orderLimitFoodCount &&
      updateContractDto.orderLimitFoodCount != targetContract.orderLimitFoodCount
    )
      return true;
    if (
      updateContractDto.orderLimitTime &&
      updateContractDto.orderLimitTime != targetContract.orderLimitTime
    )
      return true;

    // if statuses or minor changes then should not change other party status to pending
    return false;
  }
}
