import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

import { USER_TYPES } from './../../utils/constants';

export class SignInDto {
  @ApiProperty({
    example: 'testEmployee',
    required: true,
  })
  @IsNotEmpty({ message: 'Please enter username' })
  username: string;

  @ApiProperty({
    example: 'myPowefulPa$$001',
    required: true,
  })
  @IsNotEmpty({ message: 'Please enter password' })
  password: string;

  @ApiProperty({
    enum: USER_TYPES,
    example: `${Object.values(USER_TYPES).filter((v) => isNaN(Number(v)))}`,
    required: true,
  })
  @IsNotEmpty({ message: 'Please enter user type' })
  type: string;
}
