import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { USER_TYPES } from './../utils/constants';
import { passwordMd5Encrypt } from './../utils/functions';

import { AdminsService } from './../admins/admins.service';
import { RestaurantsService } from './../restaurants/restaurants.service';
import { OrganizationsService } from './../organizations/organizations.service';
import { OrganizationEmployeesService } from './../organization-employees/organization-employees.service';
import { UsersService } from './../users/users.service';
import { SignInDto } from './dto/sign-in.dto';

@Injectable()
export class AuthService {
  constructor(
    private adminService: AdminsService,
    private restaurantService: RestaurantsService,
    private organziationService: OrganizationsService,
    private employeeService: OrganizationEmployeesService,
    private userService: UsersService,
    private jwtService: JwtService,
  ) {}

  async signIn(signInDto: SignInDto): Promise<{ access_token: string }> {
    let user;
    switch (signInDto.type) {
      case USER_TYPES.RESTAURANT:
        user = await this.restaurantService.findOneByUsername(signInDto.username);
        break;
      case USER_TYPES.ORGANIZATION:
        user = await this.organziationService.findOneByUsername(signInDto.username);
        break;
      case USER_TYPES.EMPLOYEE:
        user = await this.employeeService.findOneByUsername(signInDto.username);
        break;
      case USER_TYPES.USER:
        user = await this.userService.findOneByUsername(signInDto.username);
        break;
      case USER_TYPES.ADMIN:
        user = await this.adminService.findOneByUsername(signInDto.username);
        break;
      default:
        throw new UnauthorizedException();
    }

    const password = await passwordMd5Encrypt(signInDto.password);
    if (user?.password !== password) {
      throw new UnauthorizedException();
    }
    const payload = { id: user.id, username: user.username, type: signInDto.type };
    return {
      access_token: await this.jwtService.signAsync(payload),
    };
  }

  async getUser(userId: string, type: string): Promise<{ user }> {
    let user;
    switch (type) {
      case USER_TYPES.RESTAURANT:
        user = await this.restaurantService.findOneById(userId);
        break;
      case USER_TYPES.ORGANIZATION:
        user = await this.organziationService.findOneById(userId);
        break;
      case USER_TYPES.EMPLOYEE:
        user = await this.employeeService.findOneById(userId);
        break;
      case USER_TYPES.USER:
        user = await this.userService.findOneById(userId);
        break;
      case USER_TYPES.ADMIN:
        user = await this.adminService.findOneById(userId);
        break;
      default:
        throw new UnauthorizedException();
    }
    return { ...user, type };
  }
}
