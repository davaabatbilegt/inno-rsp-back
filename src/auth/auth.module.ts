import { APP_GUARD } from '@nestjs/core/constants';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { AdminsModule } from './../admins/admins.module';
import { OrganizationsModule } from './../organizations/organizations.module';
import { OrganizationEmployeesModule } from './../organization-employees/organization-employees.module';
import { RestaurantsModule } from './../restaurants/restaurants.module';
import { UsersModule } from './../users/users.module';

import { AuthGuard } from './auth.guard';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

@Module({
  imports: [
    ConfigModule,
    AdminsModule,
    OrganizationsModule,
    OrganizationEmployeesModule,
    RestaurantsModule,
    UsersModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET'),
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [
    AuthService,
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
  controllers: [AuthController],
  exports: [AuthService],
})
export class AuthModule {}
