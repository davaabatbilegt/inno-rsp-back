import { Body, Controller, Post, HttpCode, HttpStatus, Request, Get } from '@nestjs/common';
import { ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';

import {
  AdminAccess,
  EmployeeAccess,
  OrganizationAccess,
  Public,
  RestaurantAccess,
  UserAccess,
} from './../decorators/guard.decorator';

import { SignInDto } from './dto/sign-in.dto';
import { AuthService } from './auth.service';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Public()
  @ApiResponse({ status: 200, description: 'Sign in successfully.' })
  @ApiBody({
    type: SignInDto,
    description: 'Json structure for sign-in object',
  })
  @HttpCode(HttpStatus.OK)
  @Post('login')
  signIn(@Body() signInDto: SignInDto) {
    return this.authService.signIn(signInDto);
  }

  @AdminAccess()
  @EmployeeAccess()
  @RestaurantAccess()
  @OrganizationAccess()
  @UserAccess()
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @Get('profile')
  getProfile(@Request() req) {
    return this.authService.getUser(req.user.id, req.user.type);
  }
}
