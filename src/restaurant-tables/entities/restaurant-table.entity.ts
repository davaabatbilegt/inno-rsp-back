import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { STATUSES } from './../../utils/constants';

import { Restaurant } from './../../restaurants/entities/restaurant.entity';
import { RestaurantOrder } from './../../restaurant-orders/entities/restaurant-order.entity';

@Entity({ name: 'restaurant_tables' })
export class RestaurantTable {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ name: 'restaurant_id' })
  restaurantId: number;

  @Column()
  name: string;

  @Column({
    type: 'enum',
    enum: STATUSES,
    default: STATUSES.ACTIVE,
    transformer: {
      to: (value: STATUSES) => STATUSES[value],
      from: (value: number) => STATUSES[value],
    },
  })
  status: string;

  @Column()
  capacity: number;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @ManyToOne(() => Restaurant, (restaurant) => restaurant.contracts, { cascade: true, eager: true })
  @JoinColumn({ name: 'restaurant_id' })
  restaurant: Restaurant;

  @OneToMany(() => RestaurantOrder, (order) => order.table)
  orders: RestaurantOrder[];

  constructor(partial: Partial<RestaurantTable>) {
    Object.assign(this, partial);
  }
}
