import {
  Controller,
  Get,
  Param,
  Request,
  UseInterceptors,
  ClassSerializerInterceptor,
  Query,
} from '@nestjs/common';
import { ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';

import {
  AdminAccess,
  EmployeeAccess,
  OrganizationAccess,
  RestaurantAccess,
  UserAccess,
} from './../decorators/guard.decorator';
import { validatePagination, validateSortOrder, validateStatus } from './../utils/functions';
import { SORT_ORDER, STATUSES } from './../utils/constants';

import { RestaurantTablesService } from './restaurant-tables.service';
// import { CreateRestaurantTableDto } from './dto/create-restaurant-table.dto';
// import { UpdateRestaurantTableDto } from './dto/update-restaurant-table.dto';
import { RestaurantTable } from './entities/restaurant-table.entity';

const prefix = 'restaurant-tables';

@ApiTags(prefix)
@ApiResponse({ status: 401, description: 'Unauthorized.' })
@UseInterceptors(ClassSerializerInterceptor)
@Controller(prefix)
@Controller('restaurant-tables')
export class RestaurantTablesController {
  constructor(private readonly restaurantTablesService: RestaurantTablesService) {}

  // @Post()
  // create(@Body() createRestaurantTableDto: CreateRestaurantTableDto) {
  //   return this.restaurantTablesService.create(createRestaurantTableDto);
  // }

  @AdminAccess()
  @EmployeeAccess()
  @RestaurantAccess()
  @OrganizationAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @ApiQuery({
    name: 'restaurantId',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'status',
    required: false,
    type: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'page',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: `${Object.values(SORT_ORDER).filter((v) => isNaN(Number(v)))}`,
  })
  @Get()
  findAll(
    @Request() req,
    @Query('restaurantId') restaurantId: number,
    @Query('status') status: string | STATUSES,
    @Query() options: IPaginationOptions,
    @Query('sortOrder') sortOrder: SORT_ORDER,
  ): Promise<Pagination<RestaurantTable>> {
    validatePagination(options, prefix);
    sortOrder = validateSortOrder(sortOrder);
    status = validateStatus(status);

    return this.restaurantTablesService.findAll(
      restaurantId,
      status,
      options,
      sortOrder,
      req.user.id,
      req.user.type,
    );
  }

  @AdminAccess()
  @EmployeeAccess()
  @RestaurantAccess()
  @OrganizationAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @Get(':id')
  findOne(@Request() req, @Param('id') id: string) {
    return this.restaurantTablesService.findOne(+id, req.user.id, req.user.type);
  }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateRestaurantTableDto: UpdateRestaurantTableDto) {
  //   return this.restaurantTablesService.update(+id, updateRestaurantTableDto);
  // }
}
