import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IPaginationOptions, Pagination, paginate } from 'nestjs-typeorm-paginate';

import { SORT_ORDER, STATUSES, USER_TYPES } from './../utils/constants';
import { restaurantCondition } from './../utils/functions';

// import { CreateRestaurantTableDto } from './dto/create-restaurant-table.dto';
// import { UpdateRestaurantTableDto } from './dto/update-restaurant-table.dto';
import { RestaurantTable } from './entities/restaurant-table.entity';

@Injectable()
export class RestaurantTablesService {
  constructor(
    @InjectRepository(RestaurantTable)
    private restaurantTableRepository: Repository<RestaurantTable>,
  ) {}

  // create(createRestaurantTableDto: CreateRestaurantTableDto) {
  //   console.log(createRestaurantTableDto);
  //   return 'This action adds a new restaurantTable';
  // }

  async findAll(
    restaurantId: number,
    status: string | STATUSES,
    options: IPaginationOptions,
    sortOrder: SORT_ORDER,
    userId: string,
    type: string,
  ): Promise<Pagination<RestaurantTable>> {
    const queryBuilder = this.restaurantTableRepository.createQueryBuilder('rt');

    if (type == USER_TYPES.RESTAURANT) {
      queryBuilder.andWhere({ restaurantId: userId });
    }

    if (restaurantId && type != USER_TYPES.RESTAURANT) {
      queryBuilder.andWhere({ restaurantId: restaurantId });
    }

    if (status) {
      queryBuilder.andWhere({ status: status });
    }

    queryBuilder.leftJoinAndSelect('rt.restaurant', 'r').orderBy('rt.name', sortOrder);

    return paginate<RestaurantTable>(queryBuilder, options);
  }

  async findOne(id: number, userId: string, type: string): Promise<RestaurantTable> {
    let condition = null;
    condition = restaurantCondition(userId, type);

    const restaurantTable = await this.restaurantTableRepository.findOne({
      where: {
        id: id,
        ...condition,
      },
    });

    if (!restaurantTable) throw new UnauthorizedException('error.restaurantTable not found');

    return restaurantTable;
  }

  // update(id: number, updateRestaurantTableDto: UpdateRestaurantTableDto) {
  //   console.log(updateRestaurantTableDto);
  //   return `This action updates a #${id} restaurantTable`;
  // }
}
