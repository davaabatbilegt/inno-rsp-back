import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

import { STATUSES } from './../../utils/constants';

export class CreateRestaurantTableDto {
  @ApiProperty({
    example: 1,
    required: true,
  })
  restaurantId: number;

  @ApiProperty({
    example: 'tesTableName',
    required: true,
  })
  @IsNotEmpty({ message: 'Please enter table name' })
  name: string;

  @ApiProperty({
    enum: STATUSES,
    example: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
    required: false,
  })
  status: string;

  @ApiProperty({
    example: 4,
    required: false,
  })
  capacity: number;
}
