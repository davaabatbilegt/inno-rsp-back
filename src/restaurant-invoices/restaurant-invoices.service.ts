import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IPaginationOptions, Pagination, paginate } from 'nestjs-typeorm-paginate';

import {
  BILL_STATUSES,
  ORDER_IN_CONTRACTS,
  SORT_ORDER,
  STATUSES,
  USER_TYPES,
} from './../utils/constants';
import { allUserTypeCondition } from './../utils/functions';

import { CreateRestaurantInvoiceDto } from './dto/create-restaurant-invoice.dto';
import { UpdateRestaurantInvoiceDto } from './dto/update-restaurant-invoice.dto';
import { RestaurantInvoice } from './entities/restaurant-invoice.entity';

import { RestaurantInvoiceDetailsService } from './../restaurant-invoice-details/restaurant-invoice-details.service';
import { RestaurantOrderDetailsService } from './../restaurant-order-details/restaurant-order-details.service';
import { CreateRestaurantInvoiceDetailDto } from './../restaurant-invoice-details/dto/create-restaurant-invoice-detail.dto';

@Injectable()
export class RestaurantInvoicesService {
  constructor(
    @InjectRepository(RestaurantInvoice)
    private restaurantInvoiceRepository: Repository<RestaurantInvoice>,
    private restaurantInvoiceDetailsService: RestaurantInvoiceDetailsService,
    private restaurantOrderDetailsService: RestaurantOrderDetailsService,
  ) {}

  async create(
    createRestaurantInvoiceDto: CreateRestaurantInvoiceDto,
    userId: string,
  ): Promise<RestaurantInvoice | undefined> {
    try {
      createRestaurantInvoiceDto.restaurantId = parseInt(userId, 10);

      const restaurantInvoice = await this.findOneByParam(
        createRestaurantInvoiceDto.yearMonth,
        createRestaurantInvoiceDto.organizationId,
        createRestaurantInvoiceDto.restaurantId,
      );

      // invoice should not exists
      if (restaurantInvoice) {
        throw new UnauthorizedException('error.restaurantInvoice exists');
      }

      // calculate order details and total bill
      return await this.createAndSaveInvoiceDetails(createRestaurantInvoiceDto);
    } catch (ex) {
      throw new BadRequestException(`Create error: ${ex.message}`);
    }
  }

  async findAll(
    restaurantId: string,
    organizationId: string,
    billStatus: string | BILL_STATUSES,
    yearMonth: string,
    status: string | STATUSES,
    options: IPaginationOptions,
    sortOrder: SORT_ORDER,
    userId: string,
    type: string,
  ): Promise<Pagination<RestaurantInvoice>> {
    const queryBuilder = this.restaurantInvoiceRepository.createQueryBuilder('ri');

    if (type == USER_TYPES.RESTAURANT) {
      queryBuilder.andWhere({ restaurantId: userId });
    } else if (restaurantId) {
      queryBuilder.andWhere({ restaurantId: restaurantId });
    }

    if (type == USER_TYPES.ORGANIZATION) {
      queryBuilder.andWhere({ organizationId: userId });
    } else if (organizationId) {
      queryBuilder.andWhere({ organizationId: organizationId });
    }

    if (yearMonth) {
      queryBuilder.andWhere({ yearMonth: yearMonth });
    }

    if (billStatus) {
      queryBuilder.andWhere({ billStatus: billStatus });
    }

    if (status) {
      queryBuilder
        .andWhere({ status: status })
        .andWhere('rid.status = :status', { status: status });
    }

    queryBuilder
      .leftJoinAndSelect('ri.invoiceDetails', 'rid')
      .leftJoinAndSelect('ri.organization', 'o')
      .leftJoinAndSelect('ri.restaurant', 'r')
      .orderBy('ri.yearMonth', sortOrder);

    return paginate<RestaurantInvoice>(queryBuilder, options);
  }

  async findOne(id: number, userId: string, type: string): Promise<RestaurantInvoice> {
    const condition = allUserTypeCondition(userId, type);
    const queryBuilder = this.restaurantInvoiceRepository.createQueryBuilder('ri');

    const restaurantInvoice = queryBuilder
      .andWhere({ id: id })
      .andWhere(condition)
      .leftJoinAndSelect('ri.invoiceDetails', 'rid')
      .leftJoinAndSelect('ri.organization', 'o')
      .leftJoinAndSelect('ri.restaurant', 'r')
      .getOne();

    if (!restaurantInvoice) throw new UnauthorizedException('error.restaurantInvoice not found');

    return restaurantInvoice;
  }

  async findOneByParam(yearMonth: Date, organizationId: number, restaurantId: number) {
    const restaurantInvoice = await this.restaurantInvoiceRepository.findOne({
      where: {
        yearMonth: yearMonth,
        organizationId: organizationId,
        restaurantId: restaurantId,
      },
    });

    return restaurantInvoice;
  }

  async update(
    id: number,
    updateRestaurantInvoiceDto: UpdateRestaurantInvoiceDto,
    userId: string,
    type: string,
  ): Promise<RestaurantInvoice | undefined> {
    if (!id) throw new BadRequestException(`Update error: id is empty.`);
    updateRestaurantInvoiceDto.id = id;

    try {
      // Invoice should belongs logged user
      const targetInvoice = await this.findOne(id, userId, type);
      if (!targetInvoice) {
        throw new BadRequestException(`Update error: Invoice not belongs to user`);
      }

      if (
        updateRestaurantInvoiceDto.billStatus ==
        Object.values(BILL_STATUSES)[BILL_STATUSES.CONFIRMED]
      ) {
        // update all order to billed, but we cannot update enum -> should be checked
        await this.restaurantOrderDetailsService.updateToBilledTargetMonthOrganizationOrders(
          targetInvoice.yearMonth,
          targetInvoice.restaurantId,
          targetInvoice.organizationId,
        );
      }
      return this.restaurantInvoiceRepository.save(updateRestaurantInvoiceDto);
    } catch (ex) {
      throw new BadRequestException(`Update error: ${ex.message}`);
    }
  }

  async createAndSaveInvoiceDetails(
    createRestaurantInvoiceDto: CreateRestaurantInvoiceDto,
  ): Promise<RestaurantInvoice> {
    const orderDetails = await this.restaurantOrderDetailsService.getTargetMonthOrganizationOrders(
      createRestaurantInvoiceDto.yearMonth,
      createRestaurantInvoiceDto.restaurantId,
      createRestaurantInvoiceDto.organizationId,
    );

    const menuUnitCountMap = new Map();
    const menuTotalPriceMap = new Map();
    let totalBill = 0;

    // calculate per menu items
    orderDetails.forEach((orderDetail) => {
      if (
        orderDetail.isInsideContractRange ==
        Object.values(ORDER_IN_CONTRACTS)[ORDER_IN_CONTRACTS.IN_CONTRACT]
      ) {
        if (menuUnitCountMap.get(orderDetail.restaurantMenuId) == undefined) {
          menuUnitCountMap.set(orderDetail.restaurantMenuId, orderDetail.unitCount);
        } else {
          menuUnitCountMap.set(
            orderDetail.restaurantMenuId,
            menuUnitCountMap.get(orderDetail.restaurantMenuId) + orderDetail.unitCount,
          );
        }

        if (menuTotalPriceMap.get(orderDetail.restaurantMenuId) == undefined) {
          menuTotalPriceMap.set(orderDetail.restaurantMenuId, orderDetail.totalPrice);
        } else {
          menuTotalPriceMap.set(
            orderDetail.restaurantMenuId,
            menuTotalPriceMap.get(orderDetail.restaurantMenuId) + orderDetail.totalPrice,
          );
        }

        totalBill = totalBill + orderDetail.totalPrice;
      }
    });

    // set invoice total Bill
    createRestaurantInvoiceDto.totalMoney = totalBill;

    const restaurantInvoice = await this.restaurantInvoiceRepository.save(
      createRestaurantInvoiceDto,
    );

    // save invoice details and calculate total
    for (const key of menuUnitCountMap.keys()) {
      const createRestaurantInvoiceDetailDto = new CreateRestaurantInvoiceDetailDto();

      createRestaurantInvoiceDetailDto.organizationId = restaurantInvoice.organizationId;
      createRestaurantInvoiceDetailDto.restaurantId = restaurantInvoice.restaurantId;
      createRestaurantInvoiceDetailDto.restaurantInvoiceId = restaurantInvoice.id;
      createRestaurantInvoiceDetailDto.yearMonth = restaurantInvoice.yearMonth;
      createRestaurantInvoiceDetailDto.restaurantMenuId = key;
      createRestaurantInvoiceDetailDto.unitCount = menuUnitCountMap.get(key);
      createRestaurantInvoiceDetailDto.totalPrice = menuTotalPriceMap.get(key);
      createRestaurantInvoiceDetailDto.unitPrice =
        menuTotalPriceMap.get(key) / menuUnitCountMap.get(key);

      this.restaurantInvoiceDetailsService.createRestaurantInvoiceDetail(
        createRestaurantInvoiceDetailDto,
      );
    }

    return restaurantInvoice;
  }
}
