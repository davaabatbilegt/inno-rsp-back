import {
  Controller,
  Get,
  Param,
  Query,
  Request,
  UseInterceptors,
  ClassSerializerInterceptor,
  Post,
  Body,
  Patch,
} from '@nestjs/common';
import { ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';

import { OrganizationAccess, RestaurantAccess } from './../decorators/guard.decorator';
import {
  validateBillStatus,
  validatePagination,
  validateSortOrder,
  validateStatus,
} from './../utils/functions';
import { BILL_STATUSES, SORT_ORDER, STATUSES } from './../utils/constants';

import { RestaurantInvoicesService } from './restaurant-invoices.service';
import { CreateRestaurantInvoiceDto } from './dto/create-restaurant-invoice.dto';
import { UpdateRestaurantInvoiceDto } from './dto/update-restaurant-invoice.dto';
import { RestaurantInvoice } from './entities/restaurant-invoice.entity';

const prefix = 'restaurant-invoices';

@ApiTags(prefix)
@ApiResponse({ status: 401, description: 'Unauthorized.' })
@UseInterceptors(ClassSerializerInterceptor)
@Controller(prefix)
export class RestaurantInvoicesController {
  constructor(private readonly restaurantInvoicesService: RestaurantInvoicesService) {}

  @RestaurantAccess()
  @Post()
  create(@Request() req, @Body() createRestaurantInvoiceDto: CreateRestaurantInvoiceDto) {
    return this.restaurantInvoicesService.create(createRestaurantInvoiceDto, req.user.id);
  }

  @RestaurantAccess()
  @OrganizationAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @ApiQuery({
    name: 'restaurantId',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'organizationId',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'billStatus',
    required: false,
    type: `${Object.values(BILL_STATUSES).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'yearMonth',
    required: false,
    type: Date,
  })
  @ApiQuery({
    name: 'status',
    required: false,
    type: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'page',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: `${Object.values(SORT_ORDER).filter((v) => isNaN(Number(v)))}`,
  })
  @Get()
  findAll(
    @Request() req,
    @Query('restaurantId') restaurantId: string,
    @Query('organizationId') organizationId: string,
    @Query('billStatus') billStatus: string | BILL_STATUSES,
    @Query('yearMonth') yearMonth: string,
    @Query('status') status: string | STATUSES,
    @Query() options: IPaginationOptions,
    @Query('sortOrder') sortOrder: SORT_ORDER,
  ): Promise<Pagination<RestaurantInvoice>> {
    validatePagination(options, prefix);
    sortOrder = validateSortOrder(sortOrder);
    billStatus = validateBillStatus(billStatus);
    status = validateStatus(status);

    return this.restaurantInvoicesService.findAll(
      restaurantId,
      organizationId,
      billStatus,
      yearMonth,
      status,
      options,
      sortOrder,
      req.user.id,
      req.user.type,
    );
  }

  @RestaurantAccess()
  @OrganizationAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @Get(':id')
  findOne(@Request() req, @Param('id') id: string) {
    return this.restaurantInvoicesService.findOne(+id, req.user.id, req.user.type);
  }

  @RestaurantAccess()
  @OrganizationAccess()
  @Patch(':id')
  update(
    @Request() req,
    @Param('id') id: string,
    @Body() updateRestaurantInvoiceDto: UpdateRestaurantInvoiceDto,
  ) {
    return this.restaurantInvoicesService.update(
      +id,
      updateRestaurantInvoiceDto,
      req.user.id,
      req.user.type,
    );
  }
}
