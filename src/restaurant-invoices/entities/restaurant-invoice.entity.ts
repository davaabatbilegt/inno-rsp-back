import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { BILL_STATUSES, STATUSES } from './../../utils/constants';

import { Restaurant } from './../../restaurants/entities/restaurant.entity';
import { Organization } from './../../organizations/entities/organization.entity';
import { RestaurantInvoiceDetail } from './../../restaurant-invoice-details/entities/restaurant-invoice-detail.entity';

@Entity({ name: 'restaurant_invoices' })
export class RestaurantInvoice {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ name: 'restaurant_id' })
  restaurantId: number;

  @Column({ name: 'organization_id' })
  organizationId: number;

  @Column({ name: 'year_month' })
  yearMonth: Date;

  @Column({
    type: 'enum',
    enum: STATUSES,
    default: STATUSES.ACTIVE,
    transformer: {
      to: (value: STATUSES) => STATUSES[value],
      from: (value: number) => STATUSES[value],
    },
  })
  status: string;

  @Column({
    name: 'bill_status',
    type: 'enum',
    enum: BILL_STATUSES,
    default: BILL_STATUSES.CALCULATED,
    transformer: {
      to: (value: BILL_STATUSES) => BILL_STATUSES[value],
      from: (value: number) => BILL_STATUSES[value],
    },
  })
  billStatus: string;

  @Column({ name: 'total_money' })
  totalMoney: number;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  // Many to One cascade
  @ManyToOne(() => Restaurant, (restaurant) => restaurant.contracts, { cascade: true, eager: true })
  @JoinColumn({ name: 'restaurant_id' })
  restaurant: Restaurant;

  @ManyToOne(() => Organization, (organization) => organization.contracts, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: 'organization_id' })
  organization: Organization;

  // One to Many cascade
  @OneToMany(() => RestaurantInvoiceDetail, (invoiceDetail) => invoiceDetail.restaurantInvoice)
  invoiceDetails: RestaurantInvoiceDetail[];

  constructor(partial: Partial<RestaurantInvoice>) {
    Object.assign(this, partial);
  }
}
