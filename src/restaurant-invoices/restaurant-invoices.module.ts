import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { RestaurantInvoicesService } from './restaurant-invoices.service';
import { RestaurantInvoicesController } from './restaurant-invoices.controller';
import { RestaurantInvoice } from './entities/restaurant-invoice.entity';

import { RestaurantInvoiceDetailsModule } from './../restaurant-invoice-details/restaurant-invoice-details.module';
import { RestaurantOrderDetailsModule } from './../restaurant-order-details/restaurant-order-details.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([RestaurantInvoice]),
    RestaurantInvoiceDetailsModule,
    RestaurantOrderDetailsModule,
  ],
  controllers: [RestaurantInvoicesController],
  providers: [RestaurantInvoicesService],
  exports: [RestaurantInvoicesService],
})
export class RestaurantInvoicesModule {}
