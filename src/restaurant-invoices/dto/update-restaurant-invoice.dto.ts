import { PartialType } from '@nestjs/swagger';

import { CreateRestaurantInvoiceDto } from './create-restaurant-invoice.dto';

export class UpdateRestaurantInvoiceDto extends PartialType(CreateRestaurantInvoiceDto) {
  id: number;
}
