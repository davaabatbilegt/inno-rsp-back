import { ApiProperty } from '@nestjs/swagger';

import { BILL_STATUSES, STATUSES } from './../../utils/constants';

export class CreateRestaurantInvoiceDto {
  @ApiProperty({
    example: 1,
    required: false,
  })
  restaurantId: number;

  @ApiProperty({
    example: 1,
    required: false,
  })
  organizationId: number;

  @ApiProperty({
    example: '2024/03',
    required: true,
  })
  yearMonth: Date;

  @ApiProperty({
    enum: STATUSES,
    example: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
    required: false,
  })
  status: string;

  @ApiProperty({
    enum: BILL_STATUSES,
    example: `${Object.values(BILL_STATUSES).filter((v) => isNaN(Number(v)))}`,
    required: false,
  })
  billStatus: string;

  @ApiProperty({
    example: 1500000,
    required: true,
  })
  totalMoney: number;
}
