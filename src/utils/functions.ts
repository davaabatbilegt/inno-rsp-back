import { UnauthorizedException } from '@nestjs/common';

import {
  BILL_STATUSES,
  CONTRACT_STATUSES,
  MENU_DAYS,
  MONTH_KEYS,
  ORDER_IN_CONTRACTS,
  ORDER_STATUS,
  SORT_ORDER,
  STATUSES,
  USER_TYPES,
} from './../utils/constants';
import { IPaginationOptions } from 'nestjs-typeorm-paginate';
import * as moment from 'moment';

export const passwordMd5Encrypt = async (password: string): Promise<string> => {
  const crypto = await require('crypto');
  return crypto.createHash('md5').update(password).digest('hex');
};

export const allUserTypeCondition = (userId: string, type: string): string => {
  let condition = null;

  switch (type) {
    case USER_TYPES.RESTAURANT:
      condition = { restaurantId: userId };
      break;
    case USER_TYPES.ORGANIZATION:
      condition = { organizationId: userId };
      break;
    case USER_TYPES.EMPLOYEE:
      condition = { employeeId: userId };
      break;
    case USER_TYPES.USER:
      condition = { userId: userId };
      break;
    case USER_TYPES.ADMIN:
      // no condition
      break;
    default:
      throw new UnauthorizedException();
  }

  return condition;
};

export const restaurantCondition = (userId: string, type: string): string => {
  let condition = null;

  if (type == USER_TYPES.RESTAURANT) {
    condition = { restaurantId: userId };
  }

  return condition;
};

export const organizationCondition = (userId: string, type: string): string => {
  let condition = null;

  if (type == USER_TYPES.ORGANIZATION) {
    condition = { organizationId: userId };
  }

  return condition;
};

export const employeeCondition = (userId: string, type: string): string => {
  let condition = null;

  if (type == USER_TYPES.EMPLOYEE) {
    condition = { employeeId: userId };
  }

  return condition;
};

export const userCondition = (userId: string, type: string): string => {
  let condition = null;

  if (type == USER_TYPES.USER) {
    condition = { userId: userId };
  }

  return condition;
};

export const validatePagination = (options: IPaginationOptions, prefix: string): void => {
  // if undefined then set default value
  if (!options.page) {
    options.page = 1;
  }

  // if undefined then set default value
  if (!options.limit) {
    options.limit = 10;
  }

  // set contoller prefix value
  options.route = prefix;
};

export const validateSortOrder = (sortOrder: SORT_ORDER): SORT_ORDER => {
  if (!sortOrder) {
    // if undefined then set default value
    sortOrder = SORT_ORDER.ASC;
  } else if (Object.values(SORT_ORDER).includes(sortOrder) === false) {
    // if not enum value then set default value
    sortOrder = SORT_ORDER.ASC;
  }
  return sortOrder;
};

export const validateStatus = (status: string | STATUSES): string | STATUSES => {
  if (!status) {
    return null;
  } else if (Object.values(STATUSES).includes(status) === false) {
    // if not enum value then set default value
    return Object.values(STATUSES)[STATUSES.ACTIVE];
  }

  return status;
};

export const validateContractStatus = (
  contractStatus: string | CONTRACT_STATUSES,
): string | CONTRACT_STATUSES => {
  if (!contractStatus) {
    return null;
  } else if (Object.values(CONTRACT_STATUSES).includes(contractStatus) === false) {
    // if not enum value then set default value
    return Object.values(CONTRACT_STATUSES)[CONTRACT_STATUSES.ACCEPTED];
  }

  return contractStatus;
};

export const validateBillStatus = (
  contractStatus: string | BILL_STATUSES,
): string | BILL_STATUSES => {
  if (!contractStatus) {
    return null;
  } else if (Object.values(BILL_STATUSES).includes(contractStatus) === false) {
    // if not enum value then set default value
    return Object.values(BILL_STATUSES)[BILL_STATUSES.CALCULATED];
  }

  return contractStatus;
};

export const validateOrderStatus = (orderStatus: string | ORDER_STATUS): string | ORDER_STATUS => {
  if (!orderStatus) {
    return null;
  } else if (Object.values(ORDER_STATUS).includes(orderStatus) === false) {
    // if not enum value then set default value
    return Object.values(ORDER_STATUS)[ORDER_STATUS.ORDERED];
  }

  return orderStatus;
};

export const validateIsInsideContractRange = (
  isInsideContractRange: string | ORDER_IN_CONTRACTS,
): string | ORDER_IN_CONTRACTS => {
  if (!isInsideContractRange) {
    return null;
  } else if (Object.values(ORDER_IN_CONTRACTS).includes(isInsideContractRange) === false) {
    // if not enum value then set default value
    return Object.values(ORDER_IN_CONTRACTS)[ORDER_IN_CONTRACTS.IN_CONTRACT];
  }

  return isInsideContractRange;
};

export const validateMenuDays = (menuDays: string | MENU_DAYS): string | MENU_DAYS => {
  if (Object.values(MENU_DAYS).includes(Number(menuDays))) {
    // return Object.values(MENU_DAYS)[Number(menuDays)];
    return menuDays;
  }

  return null;
};

export const getMonthStartAndEnd = (targetMonth: Date): Map<string, Date> => {
  const yearMonthMap = new Map();

  yearMonthMap.set(MONTH_KEYS.START, moment(targetMonth).startOf('month').toDate());
  yearMonthMap.set(MONTH_KEYS.END, moment(targetMonth).endOf('month').toDate());

  return yearMonthMap;
};

export const getSalaryMonthStartAndEnd = (
  targetMonth: Date,
  salaryStartDay: number,
): Map<string, Date> => {
  const yearMonthMap = new Map();

  if (salaryStartDay == 1) {
    yearMonthMap.set(MONTH_KEYS.START, moment(targetMonth).startOf('month').toDate());
    yearMonthMap.set(MONTH_KEYS.END, moment(targetMonth).endOf('month').toDate());
  } else {
    yearMonthMap.set(
      MONTH_KEYS.START,
      moment(targetMonth).startOf('month').subtract(1).date(salaryStartDay).toDate(),
    );
    yearMonthMap.set(
      MONTH_KEYS.END,
      moment(targetMonth)
        .startOf('month')
        .date(salaryStartDay - 1)
        .toDate(),
    );
  }

  return yearMonthMap;
};
