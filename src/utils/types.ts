import { USER_TYPES } from './constants';

export type JwtPayload = {
  id: string;
  username: string;
  type: USER_TYPES;
};
