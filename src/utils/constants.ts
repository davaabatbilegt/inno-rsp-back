export const IS_PUBLIC_KEY = 'isPublic';

export const TYPES_KEY = 'type';

export const FRONTEND_BASE_URL = 'FRONTEND_BASE_URL';

export const GOOGLE_TOKEN_API_URL = 'https://oauth2.googleapis.com/token';

export enum USER_TYPES {
  ADMIN = 'admin',
  RESTAURANT = 'restaurant',
  ORGANIZATION = 'organization',
  EMPLOYEE = 'employee',
  USER = 'user',
}

export enum GOOGLE_CREDENTIALS {
  CLIENT_ID = 'GOOGLE_CLIENT_ID',
  CLIENT_SECRET = 'GOOGLE_CLIENT_SECRET',
  ADMIN_REDIRECT_URI = 'GOOGLE_REDIRECT_URI_ADMIN',
  CORPORATE_REDIRECT_URI = 'GOOGLE_REDIRECT_URI_CORPORATE',
}

export enum APP_ENV {
  PROD = 'prod',
  STG = 'stage',
  DEV = 'dev',
  LOCAL = 'local',
}

export enum COOKIE_NAMES {
  ACCESS_TOKEN = 'rsp_access_token',
}

// Sort order
export enum SORT_ORDER {
  ASC = 'ASC',
  DESC = 'DESC',
}

// Entity enums
export enum STATUSES {
  INACTIVE = 0,
  ACTIVE,
}

export enum CONTRACT_STATUSES {
  PENDING = 0,
  ACCEPTED,
}

export enum BILL_STATUSES {
  CALCULATED = 0,
  NOT_PAID,
  BILLED,
  CONFIRMED,
}

export enum MENU_DAYS {
  ALL = 0,
  MONDAY,
  TUESDAY,
  WEDNESDAY,
  THURSDAY,
  FRIDAY,
  SATURDAY,
  SUNDAY,
}

export enum ORDER_IN_CONTRACTS {
  NON_CONTRACT = 0,
  IN_CONTRACT,
}

export enum ORDER_STATUS {
  ORDERED = 0,
  ARRIVED,
  DELIVERED,
  BILLED,
}

export enum MONTH_KEYS {
  START = 'start',
  END = 'end',
}
