import { registerAs } from '@nestjs/config';
import { join } from 'path';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';

import { Admin } from './../admins/entities/admin.entity';
import { Organization } from './../organizations/entities/organization.entity';
import { OrganizationEmployee } from './../organization-employees/entities/organization-employee.entity';
import { Restaurant } from './../restaurants/entities/restaurant.entity';
import { RestaurantInvoiceDetail } from './../restaurant-invoice-details/entities/restaurant-invoice-detail.entity';
import { RestaurantInvoice } from './../restaurant-invoices/entities/restaurant-invoice.entity';
import { RestaurantMenu } from './../restaurant-menus/entities/restaurant-menu.entity';
import { RestaurantMenuType } from './../restaurant-menu-types/entities/restaurant-menu-type.entity';
import { RestaurantOrder } from './../restaurant-orders/entities/restaurant-order.entity';
import { RestaurantOrderDetail } from './../restaurant-order-details/entities/restaurant-order-detail.entity';
import { RestaurantOrganizationContract } from './../restaurant-organization-contracts/entities/restaurant-organization-contract.entity';
import { RestaurantTable } from './../restaurant-tables/entities/restaurant-table.entity';
import { User } from './../users/entities/user.entity';

export default registerAs(
  'database',
  (): MysqlConnectionOptions =>
    ({
      type: 'mysql',
      host: process.env.DATABASE_HOST,
      port: parseInt(process.env.DATABASE_PORT, 10),
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_DATABASE,
      entities: [
        Admin,
        Organization,
        OrganizationEmployee,
        Restaurant,
        RestaurantInvoiceDetail,
        RestaurantInvoice,
        RestaurantMenu,
        RestaurantMenuType,
        RestaurantOrder,
        RestaurantOrderDetail,
        RestaurantOrganizationContract,
        RestaurantTable,
        User,
      ],
      timezone: 'Z', // UTC_O
      synchronize: false,
      dropSchema: false,
      migrationsRun: false,
      autoLoadEntities: true,
      logging: ['query', 'warn', 'error'],
      logger: 'advanced-console',
      migrations: [join(__dirname, 'src/migrations/*{.ts,.js}')],
    }) as MysqlConnectionOptions,
);
