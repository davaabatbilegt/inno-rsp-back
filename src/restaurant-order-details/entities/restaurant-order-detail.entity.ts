import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { ORDER_IN_CONTRACTS, ORDER_STATUS, STATUSES } from './../../utils/constants';

import { Restaurant } from './../../restaurants/entities/restaurant.entity';
import { RestaurantMenu } from './../../restaurant-menus/entities/restaurant-menu.entity';
import { RestaurantOrder } from './../../restaurant-orders/entities/restaurant-order.entity';
import { Organization } from './../../organizations/entities/organization.entity';
import { OrganizationEmployee } from './../../organization-employees/entities/organization-employee.entity';
import { User } from './../../users/entities/user.entity';

@Entity({ name: 'restaurant_order_details' })
export class RestaurantOrderDetail {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ name: 'restaurant_id' })
  restaurantId: number;

  @Column({ name: 'restaurant_order_id' })
  restaurantOrderId: number;

  @Column({ name: 'restaurant_menu_id' })
  restaurantMenuId: number;

  @Column({ name: 'organization_id' })
  organizationId: number;

  @Column({ name: 'employee_id' })
  employeeId: number;

  @Column({ name: 'user_id' })
  userId: number;

  @Column({
    name: 'is_inside_contract_range',
    type: 'enum',
    enum: ORDER_IN_CONTRACTS,
    default: ORDER_IN_CONTRACTS.NON_CONTRACT,
    transformer: {
      to: (value: ORDER_IN_CONTRACTS) => ORDER_IN_CONTRACTS[value],
      from: (value: number) => ORDER_IN_CONTRACTS[value],
    },
  })
  isInsideContractRange: string;

  @Column({ name: 'order_date' })
  orderDate: Date;

  @Column({
    name: 'order_status',
    type: 'enum',
    enum: ORDER_STATUS,
    default: ORDER_STATUS.ORDERED,
    transformer: {
      to: (value: ORDER_STATUS) => ORDER_STATUS[value],
      from: (value: number) => ORDER_STATUS[value],
    },
  })
  orderStatus: string;

  @Column({
    type: 'enum',
    enum: STATUSES,
    default: STATUSES.ACTIVE,
    transformer: {
      to: (value: STATUSES) => STATUSES[value],
      from: (value: number) => STATUSES[value],
    },
  })
  status: string;

  @Column({ name: 'unit_count' })
  unitCount: number;

  @Column({ name: 'unit_price' })
  unitPrice: number;

  @Column({ name: 'total_price' })
  totalPrice: number;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  // Many to One cascade
  @ManyToOne(() => Organization, (organization) => organization.orderDetails, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: 'organization_id' })
  organization: Organization;

  @ManyToOne(() => OrganizationEmployee, (employee) => employee.orderDetails, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: 'employee_id' })
  employee: OrganizationEmployee;

  @ManyToOne(() => Restaurant, (restaurant) => restaurant.orderDetails, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: 'restaurant_id' })
  restaurant: Restaurant;

  @ManyToOne(() => RestaurantOrder, (order) => order.orderDetails, { cascade: true, eager: true })
  @JoinColumn({ name: 'restaurant_order_id' })
  order: RestaurantOrder;

  @ManyToOne(() => RestaurantMenu, (menu) => menu.orderDetail, { cascade: true, eager: true })
  @JoinColumn({ name: 'restaurant_menu_id' })
  menu: RestaurantMenu;

  @ManyToOne(() => User, (user) => user.orders, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: 'user_id' })
  user: User;

  constructor(partial: Partial<RestaurantOrderDetail>) {
    Object.assign(this, partial);
  }
}
