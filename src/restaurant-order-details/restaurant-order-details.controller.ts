import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Request,
  UseInterceptors,
  ClassSerializerInterceptor,
  Query,
} from '@nestjs/common';
import { ApiBody, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';

import {
  EmployeeAccess,
  OrganizationAccess,
  RestaurantAccess,
  UserAccess,
} from './../decorators/guard.decorator';
import {
  validateIsInsideContractRange,
  validateOrderStatus,
  validatePagination,
  validateSortOrder,
  validateStatus,
} from './../utils/functions';
import { ORDER_IN_CONTRACTS, ORDER_STATUS, SORT_ORDER, STATUSES } from './../utils/constants';

import { RestaurantOrderDetailsService } from './restaurant-order-details.service';
import { CreateRestaurantOrderDetailDto } from './dto/create-restaurant-order-detail.dto';
import { UpdateRestaurantOrderDetailDto } from './dto/update-restaurant-order-detail.dto';
import { RestaurantOrderDetail } from './entities/restaurant-order-detail.entity';

const prefix = 'restaurant-order-details';

@ApiTags(prefix)
@ApiResponse({ status: 401, description: 'Unauthorized.' })
@UseInterceptors(ClassSerializerInterceptor)
@Controller(prefix)
export class RestaurantOrderDetailsController {
  constructor(private readonly restaurantOrderDetailsService: RestaurantOrderDetailsService) {}

  @EmployeeAccess()
  @UserAccess()
  @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
  @ApiBody({
    type: CreateRestaurantOrderDetailDto,
    description: 'Json structure for restaurant order detail object',
  })
  @Post()
  create(@Request() req, @Body() createRestaurantOrderDetailDto: CreateRestaurantOrderDetailDto) {
    return this.restaurantOrderDetailsService.create(
      createRestaurantOrderDetailDto,
      req.user.id,
      req.user.type,
    );
  }

  @RestaurantAccess()
  @OrganizationAccess()
  @EmployeeAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @ApiQuery({
    name: 'restaurantId',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'restaurantMenuId',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'organizationId',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'employeeId',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'isInsideContractRange',
    required: false,
    type: `${Object.values(ORDER_IN_CONTRACTS).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'orderDate',
    required: false,
    type: Date,
  })
  @ApiQuery({
    name: 'orderStatus',
    required: false,
    type: `${Object.values(ORDER_STATUS).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'status',
    required: false,
    type: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'page',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: `${Object.values(SORT_ORDER).filter((v) => isNaN(Number(v)))}`,
  })
  @Get()
  findAll(
    @Request() req,
    @Query('restaurantId') restaurantId: number,
    @Query('restaurantMenuId') restaurantMenuId: number,
    @Query('organizationId') organizationId: number,
    @Query('employeeId') employeeId: number,
    @Query('isInsideContractRange') isInsideContractRange: string | ORDER_IN_CONTRACTS,
    @Query('orderDate') orderDate: Date,
    @Query('orderStatus') orderStatus: string | ORDER_STATUS,
    @Query('status') status: string | STATUSES,
    @Query() options: IPaginationOptions,
    @Query('sortOrder') sortOrder: SORT_ORDER,
  ): Promise<Pagination<RestaurantOrderDetail>> {
    validatePagination(options, prefix);
    sortOrder = validateSortOrder(sortOrder);
    orderStatus = validateOrderStatus(orderStatus);
    isInsideContractRange = validateIsInsideContractRange(isInsideContractRange);
    status = validateStatus(status);

    return this.restaurantOrderDetailsService.findAll(
      restaurantId,
      restaurantMenuId,
      organizationId,
      employeeId,
      isInsideContractRange,
      orderDate,
      orderStatus,
      status,
      options,
      sortOrder,
      req.user.id,
      req.user.type,
    );
  }

  @RestaurantAccess()
  @OrganizationAccess()
  @EmployeeAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @Get(':id')
  findOne(@Request() req, @Param('id') id: string) {
    return this.restaurantOrderDetailsService.findOne(+id, req.user.id, req.user.type);
  }

  @RestaurantAccess()
  @OrganizationAccess()
  @EmployeeAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully updated.' })
  @ApiBody({
    type: UpdateRestaurantOrderDetailDto,
    description: 'Json structure for restaurant order detail object',
  })
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateRestaurantOrderDetailDto: UpdateRestaurantOrderDetailDto,
  ) {
    return this.restaurantOrderDetailsService.update(+id, updateRestaurantOrderDetailDto);
  }

  @RestaurantAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @ApiQuery({
    name: 'orderDate',
    required: false,
    type: Date,
  })
  @ApiQuery({
    name: 'restaurantMenuId',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'organizationId',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: `${Object.values(SORT_ORDER).filter((v) => isNaN(Number(v)))}`,
  })
  @Get('order/summary')
  findTargetDayOrderSummary(
    @Request() req,
    @Query('orderDate') orderDate: Date,
    @Query('restaurantMenuId') restaurantMenuId: number,
    @Query('organizationId') organizationId: number,
    @Query('sortOrder') sortOrder: SORT_ORDER,
  ) {
    sortOrder = validateSortOrder(sortOrder);

    return this.restaurantOrderDetailsService.findTargetDayOrderSummary(
      orderDate,
      restaurantMenuId,
      organizationId,
      sortOrder,
      req.user.id,
    );
  }
}
