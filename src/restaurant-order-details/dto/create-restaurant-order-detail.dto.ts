import { ApiProperty } from '@nestjs/swagger';

import { ORDER_IN_CONTRACTS, ORDER_STATUS, STATUSES } from './../../utils/constants';

export class CreateRestaurantOrderDetailDto {
  @ApiProperty({
    example: 1,
    required: true,
  })
  restaurantId: number;

  @ApiProperty({
    example: 1,
    required: false,
  })
  restaurantOrderId: number;

  @ApiProperty({
    example: 1,
    required: true,
  })
  restaurantMenuId: number;

  @ApiProperty({
    example: 1,
    required: false,
  })
  organizationId: number;

  @ApiProperty({
    example: 1,
    required: false,
  })
  employeeId: number;

  @ApiProperty({
    example: 1,
    required: false,
  })
  userId: number;

  @ApiProperty({
    enum: ORDER_IN_CONTRACTS,
    example: `${Object.values(ORDER_IN_CONTRACTS).filter((v) => isNaN(Number(v)))}`,
    required: true,
  })
  isInsideContractRange: string;

  @ApiProperty({
    example: '2024-03-10',
    required: false,
  })
  orderDate: Date;

  @ApiProperty({
    enum: ORDER_STATUS,
    example: `${Object.values(ORDER_STATUS).filter((v) => isNaN(Number(v)))}`,
    required: true,
  })
  orderStatus: string;

  @ApiProperty({
    enum: STATUSES,
    example: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
    required: false,
  })
  status: string;

  @ApiProperty({
    example: 1,
    required: true,
  })
  unitCount: number;

  @ApiProperty({
    example: 10000,
    required: true,
  })
  unitPrice: number;

  @ApiProperty({
    example: 10000,
    required: true,
  })
  totalPrice: number;
}
