import { PartialType } from '@nestjs/swagger';

import { CreateRestaurantOrderDetailDto } from './create-restaurant-order-detail.dto';

export class UpdateRestaurantOrderDetailDto extends PartialType(CreateRestaurantOrderDetailDto) {
  id: number;
}
