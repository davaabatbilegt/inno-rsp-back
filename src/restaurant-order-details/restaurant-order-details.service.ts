import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, DataSource, Repository } from 'typeorm';
import { IPaginationOptions, Pagination, paginate } from 'nestjs-typeorm-paginate';
import * as moment from 'moment';

import {
  CONTRACT_STATUSES,
  MONTH_KEYS,
  ORDER_IN_CONTRACTS,
  ORDER_STATUS,
  SORT_ORDER,
  STATUSES,
  USER_TYPES,
} from './../utils/constants';
import { allUserTypeCondition, getMonthStartAndEnd } from './../utils/functions';

import { CreateRestaurantOrderDetailDto } from './dto/create-restaurant-order-detail.dto';
import { UpdateRestaurantOrderDetailDto } from './dto/update-restaurant-order-detail.dto';
import { RestaurantOrderDetail } from './entities/restaurant-order-detail.entity';

import { OrganizationEmployee } from './../organization-employees/entities/organization-employee.entity';
import { RestaurantOrdersService } from './../restaurant-orders/restaurant-orders.service';
import { RestaurantOrder } from './../restaurant-orders/entities/restaurant-order.entity';
import { RestaurantOrganizationContract } from './../restaurant-organization-contracts/entities/restaurant-organization-contract.entity';

@Injectable()
export class RestaurantOrderDetailsService {
  constructor(
    @InjectRepository(OrganizationEmployee)
    private employeeRepository: Repository<OrganizationEmployee>,
    @InjectRepository(RestaurantOrderDetail)
    private orderDetailRepository: Repository<RestaurantOrderDetail>,
    @InjectRepository(RestaurantOrder)
    private orderRepository: Repository<RestaurantOrder>,
    @InjectRepository(RestaurantOrganizationContract)
    private contactRepository: Repository<RestaurantOrganizationContract>,
    private dataSource: DataSource,
    private restaurantOrdersService: RestaurantOrdersService,
  ) {}

  async create(
    createRestaurantOrderDetailDto: CreateRestaurantOrderDetailDto,
    userId: string,
    type: string,
  ): Promise<RestaurantOrderDetail | undefined> {
    try {
      if (type == USER_TYPES.EMPLOYEE) {
        createRestaurantOrderDetailDto.employeeId = parseInt(userId, 10);

        if (createRestaurantOrderDetailDto.restaurantId == undefined) {
          throw new BadRequestException('Restaurant not defined!');
        }

        if (createRestaurantOrderDetailDto.organizationId == undefined) {
          throw new BadRequestException('Organization not defined!');
        }

        if (createRestaurantOrderDetailDto.unitCount == undefined) {
          throw new BadRequestException('unitCount not defined!');
        }

        if (createRestaurantOrderDetailDto.restaurantMenuId == undefined) {
          throw new BadRequestException('Menu not defined!');
        }

        if (createRestaurantOrderDetailDto.isInsideContractRange == undefined) {
          throw new BadRequestException('isInsideContractRange not defined!');
        }

        if (createRestaurantOrderDetailDto.unitPrice == undefined) {
          throw new BadRequestException('unitPrice not defined!');
        }

        if (createRestaurantOrderDetailDto.totalPrice == undefined) {
          throw new BadRequestException('totalPrice not defined!');
        }

        const queryBuilderRoc = this.contactRepository.createQueryBuilder('roc');
        queryBuilderRoc
          .andWhere({
            organizationStatus: Object.values(CONTRACT_STATUSES)[CONTRACT_STATUSES.ACCEPTED],
          })
          .andWhere({
            restaurantStatus: Object.values(CONTRACT_STATUSES)[CONTRACT_STATUSES.ACCEPTED],
          })
          .andWhere({ status: Object.values(STATUSES)[STATUSES.ACTIVE] });
        const contract = await queryBuilderRoc
          .andWhere({
            restaurantId: createRestaurantOrderDetailDto.restaurantId,
            organizationId: createRestaurantOrderDetailDto.organizationId,
            menuId: createRestaurantOrderDetailDto.restaurantMenuId,
          })
          .getOne();

        if (contract == null) {
          throw new BadRequestException('Menu contract not found!');
        }

        // check order time
        const now = moment();
        // const nowTime = now.format('HH:mm:ss');
        const nowDate = now.format('YYYY-MM-DD');

        // Buruu ajillaj baij magad tul comment out hiile!
        // if (contract.orderLimitTime.toString() < nowTime) {
        //   throw new BadRequestException('Menu order time exceeded!');
        // }

        // check order limit
        let targetDate = nowDate;
        if (createRestaurantOrderDetailDto.orderDate) {
          targetDate = createRestaurantOrderDetailDto.orderDate.toString();
        }
        const queryBuilder = this.orderDetailRepository.createQueryBuilder('od');
        queryBuilder
          .select(`SUM(unit_count)`, 'totalOrderCount')
          .andWhere({
            restaurantId: createRestaurantOrderDetailDto.restaurantId,
            organizationId: createRestaurantOrderDetailDto.organizationId,
            restaurantMenuId: createRestaurantOrderDetailDto.restaurantMenuId,
            status: Object.values(STATUSES)[STATUSES.ACTIVE],
          })
          .andWhere({ orderDate: targetDate })
          .groupBy('od.restaurantMenuId');

        const orderCount = await queryBuilder.getRawMany();
        if (orderCount.length > 0) {
          if (
            +orderCount[0]['totalOrderCount'] + createRestaurantOrderDetailDto.unitCount >
            contract.orderLimitFoodCount
          ) {
            throw new BadRequestException('Menu order count exceeded!');
          }
        }
      } else if (type == USER_TYPES.USER) {
        createRestaurantOrderDetailDto.userId = parseInt(userId, 10);
      } else {
        throw new BadRequestException(`Only emloyee or user can create order detail`);
      }

      if (typeof createRestaurantOrderDetailDto.restaurantOrderId === 'undefined') {
        const activeOrder = await this.restaurantOrdersService.getActiveOrder(
          createRestaurantOrderDetailDto,
          userId,
          type,
        );

        // orderId from active order
        createRestaurantOrderDetailDto.restaurantOrderId = activeOrder.id;
      }

      return this.orderDetailRepository.save(createRestaurantOrderDetailDto);
    } catch (ex) {
      throw new BadRequestException(`Create error: ${ex.message}`);
    }
  }

  async findAll(
    restaurantId: number,
    restaurantMenuId: number,
    organizationId: number,
    employeeId: number,
    isInsideContractRange: string | ORDER_IN_CONTRACTS,
    orderDate: Date,
    orderStatus: string | ORDER_STATUS,
    status: string | STATUSES,
    options: IPaginationOptions,
    sortOrder: SORT_ORDER,
    userId: string,
    type: string,
  ): Promise<Pagination<RestaurantOrderDetail>> {
    const queryBuilder = this.orderDetailRepository.createQueryBuilder('od');

    if (type == USER_TYPES.RESTAURANT) {
      queryBuilder.andWhere({ restaurantId: userId });
    } else if (restaurantId) {
      queryBuilder.andWhere({ restaurantId: restaurantId });
    }

    if (restaurantMenuId) {
      queryBuilder.andWhere({ restaurantMenuId: restaurantMenuId });
    }

    if (type == USER_TYPES.ORGANIZATION) {
      queryBuilder.andWhere({ organizationId: userId });
    } else if (organizationId) {
      queryBuilder.andWhere({ organizationId: organizationId });
    }

    if (type == USER_TYPES.EMPLOYEE && isNaN(organizationId)) {
      const employee = await this.employeeRepository.findOne({
        where: {
          id: +userId,
        },
      });

      if (employee) {
        queryBuilder.andWhere({ organizationId: employee.organizationId });
      }
    }

    if (employeeId) {
      queryBuilder.andWhere({ employeeId: employeeId });
    }

    if (isInsideContractRange) {
      queryBuilder.andWhere({ isInsideContractRange: isInsideContractRange });
    }

    if (orderDate) {
      queryBuilder.andWhere({ orderDate: orderDate });
    }

    if (orderStatus) {
      queryBuilder.andWhere({ orderStatus: orderStatus });
    }

    if (status) {
      queryBuilder.andWhere({ status: status });
    }

    queryBuilder
      .leftJoinAndSelect('od.employee', 'e')
      .leftJoinAndSelect('od.restaurant', 'r')
      .leftJoinAndSelect('od.organization', 'o')
      .leftJoinAndSelect('od.menu', 'rm')
      .orderBy('od.createdAt', sortOrder);

    return paginate<RestaurantOrderDetail>(queryBuilder, options);
  }

  async findOne(id: number, userId: string, type: string): Promise<RestaurantOrderDetail> {
    let condition = null;
    condition = allUserTypeCondition(userId, type);

    const orderDetail = await this.orderDetailRepository.findOne({
      where: {
        id: id,
        ...condition,
      },
    });

    if (!orderDetail) throw new UnauthorizedException('error.orderDetail not found');

    return orderDetail;
  }

  update(
    id: number,
    updateRestaurantOrderDetailDto: UpdateRestaurantOrderDetailDto,
  ): Promise<RestaurantOrderDetail | undefined> {
    if (!id) throw new BadRequestException(`Update error: id is empty.`);

    try {
      updateRestaurantOrderDetailDto.id = id;

      return this.orderDetailRepository.save(updateRestaurantOrderDetailDto);
    } catch (ex) {
      throw new BadRequestException(`Update error: ${ex.message}`);
    }
  }

  async findTargetDayOrderSummary(
    orderDate: Date,
    restaurantMenuId: number,
    organizationId: number,
    sortOrder: SORT_ORDER,
    userId: string,
  ) {
    if (orderDate == undefined) {
      throw new BadRequestException('orderDate is not present!');
    }

    const queryBuilder = this.orderDetailRepository.createQueryBuilder('od');

    queryBuilder
      .andWhere({ orderDate: orderDate })
      .andWhere({
        isInsideContractRange: Object.values(ORDER_IN_CONTRACTS)[ORDER_IN_CONTRACTS.IN_CONTRACT],
      })
      .andWhere({ status: Object.values(STATUSES)[STATUSES.ACTIVE] })
      .andWhere({ restaurantId: userId });

    if (restaurantMenuId) {
      queryBuilder.andWhere({ restaurantMenuId: restaurantMenuId });
    }

    if (organizationId) {
      queryBuilder.andWhere({ organizationId: organizationId });
    }

    queryBuilder
      .select('od.order_date')
      .leftJoin('od.organization', 'o')
      .addSelect('o.name')
      .leftJoin('od.menu', 'rm')
      .addSelect('rm.name')
      .addSelect('COUNT(od.id)', 'ordered')
      .addSelect('SUM(IF(od.order_status <> 0, 1, 0))', 'delivered')
      .addSelect('SUM(IF(od.order_status = 0, 1, 0))', 'notDelivered')
      .orderBy('o.name', sortOrder)
      .groupBy('od.organization_id, od.restaurant_menu_id');

    return queryBuilder.getRawMany();
  }

  async getTargetMonthOrganizationOrders(
    targetMonth: Date,
    targetRestaurantId: number,
    targetOrganizationId: number,
  ): Promise<RestaurantOrderDetail[]> {
    const monthMap = getMonthStartAndEnd(targetMonth);

    return await this.orderDetailRepository.find({
      where: {
        restaurantId: targetRestaurantId,
        organizationId: targetOrganizationId,
        isInsideContractRange:
          Object.values(ORDER_IN_CONTRACTS)[ORDER_IN_CONTRACTS.IN_CONTRACT].toString(),
        orderStatus: Object.values(ORDER_STATUS)[ORDER_STATUS.DELIVERED].toString(),
        createdAt: Between(monthMap.get(MONTH_KEYS.START), monthMap.get(MONTH_KEYS.END)),
      },
    });
  }

  async updateToBilledTargetMonthOrganizationOrders(
    targetMonth: Date,
    targetRestaurantId: number,
    targetOrganizationId: number,
  ) {
    const monthMap = getMonthStartAndEnd(targetMonth);

    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const orders = await this.orderRepository.find({
        where: {
          restaurantId: targetRestaurantId,
          organizationId: targetOrganizationId,
          isInsideContractRange:
            Object.values(ORDER_IN_CONTRACTS)[ORDER_IN_CONTRACTS.IN_CONTRACT].toString(),
          orderStatus: Object.values(ORDER_STATUS)[ORDER_STATUS.DELIVERED].toString(),
          createdAt: Between(monthMap.get(MONTH_KEYS.START), monthMap.get(MONTH_KEYS.END)),
        },
      });

      const orderDetails = await this.getTargetMonthOrganizationOrders(
        targetMonth,
        targetRestaurantId,
        targetOrganizationId,
      );

      for (let i = 0; i < orderDetails.length; i++) {
        orderDetails[i].orderStatus = Object.values(ORDER_STATUS)[ORDER_STATUS.BILLED].toString();
        this.orderDetailRepository.save(orderDetails[i]);
      }

      // await queryRunner.manager.save(orderDetails);

      for (let j = 0; j < orders.length; j++) {
        orders[j].orderStatus = Object.values(ORDER_STATUS)[ORDER_STATUS.BILLED].toString();
        this.orderRepository.save(orders[j]);
      }

      // await queryRunner.manager.save(orders);
    } catch (e) {
      await queryRunner.rollbackTransaction();

      return false;
    } finally {
      await queryRunner.release();
    }
    return true;
  }
}
