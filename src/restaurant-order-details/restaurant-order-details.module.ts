import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { RestaurantOrderDetailsService } from './restaurant-order-details.service';
import { RestaurantOrderDetailsController } from './restaurant-order-details.controller';
import { RestaurantOrderDetail } from './entities/restaurant-order-detail.entity';

import { OrganizationEmployee } from './../organization-employees/entities/organization-employee.entity';
import { RestaurantOrdersModule } from './../restaurant-orders/restaurant-orders.module';
import { RestaurantOrder } from './../restaurant-orders/entities/restaurant-order.entity';
import { RestaurantOrganizationContract } from './../restaurant-organization-contracts/entities/restaurant-organization-contract.entity';

@Module({
  imports: [
    RestaurantOrdersModule,
    TypeOrmModule.forFeature([
      OrganizationEmployee,
      RestaurantOrderDetail,
      RestaurantOrder,
      RestaurantOrganizationContract,
    ]),
  ],
  controllers: [RestaurantOrderDetailsController],
  providers: [RestaurantOrderDetailsService],
  exports: [RestaurantOrderDetailsService],
})
export class RestaurantOrderDetailsModule {}
