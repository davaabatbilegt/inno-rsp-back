import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.enableCors({
    origin: [process.env.FRONTEND_BASE_URL, process.env.FRONTEND_ADMIN_BASE_URL],
    credentials: true,
  });

  const config = new DocumentBuilder()
    .setTitle('Restaurant Service platform REST API')
    .setDescription('Restaurant Service platform REST API Documentation')
    .setVersion('0.1.0')
    .addServer('http://localhost:3001/', 'Local environment')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);

  await app.listen(3001);
}
bootstrap();
