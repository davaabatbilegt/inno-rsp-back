import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IPaginationOptions, Pagination, paginate } from 'nestjs-typeorm-paginate';

import { SORT_ORDER, STATUSES } from './../utils/constants';

// import { CreateRestaurantMenuTypeDto } from './dto/create-restaurant-menu-type.dto';
// import { UpdateRestaurantMenuTypeDto } from './dto/update-restaurant-menu-type.dto';
import { RestaurantMenuType } from './entities/restaurant-menu-type.entity';

@Injectable()
export class RestaurantMenuTypesService {
  constructor(
    @InjectRepository(RestaurantMenuType)
    private menuTypeRepository: Repository<RestaurantMenuType>,
  ) {}

  // create(createRestaurantMenuTypeDto: CreateRestaurantMenuTypeDto) {
  //   console.log(createRestaurantMenuTypeDto);
  //   return 'This action adds a new restaurantMenuType';
  // }

  async findAll(
    status: string | STATUSES,
    options: IPaginationOptions,
    sortOrder: SORT_ORDER,
  ): Promise<Pagination<RestaurantMenuType>> {
    const queryBuilder = this.menuTypeRepository.createQueryBuilder('mt');

    if (status) {
      queryBuilder.andWhere({ status: status });
    }

    queryBuilder.orderBy('mt.name', sortOrder);

    return paginate<RestaurantMenuType>(queryBuilder, options);
  }

  async findOne(id: number): Promise<RestaurantMenuType> {
    const menuType = await this.menuTypeRepository.findOne({
      where: {
        id: id,
      },
    });

    if (!menuType) throw new UnauthorizedException('error.menuType not found');

    return menuType;
  }

  // update(id: number, updateRestaurantMenuTypeDto: UpdateRestaurantMenuTypeDto) {
  //   console.log(updateRestaurantMenuTypeDto);
  //   return `This action updates a #${id} restaurantMenuType`;
  // }
}
