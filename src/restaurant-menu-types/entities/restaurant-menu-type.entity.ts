import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { RestaurantMenu } from './../../restaurant-menus/entities/restaurant-menu.entity';
import { STATUSES } from './../../utils/constants';

@Entity({ name: 'restaurant_menu_types' })
export class RestaurantMenuType {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column({
    type: 'enum',
    enum: STATUSES,
    default: STATUSES.ACTIVE,
    transformer: {
      to: (value: STATUSES) => STATUSES[value],
      from: (value: number) => STATUSES[value],
    },
  })
  status: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @OneToMany(() => RestaurantMenu, (menu) => menu.menuType)
  menus: RestaurantMenu[];

  constructor(partial: Partial<RestaurantMenuType>) {
    Object.assign(this, partial);
  }
}
