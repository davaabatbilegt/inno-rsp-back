import {
  Controller,
  Get,
  Param,
  UseInterceptors,
  ClassSerializerInterceptor,
  Request,
  Query,
} from '@nestjs/common';
import { ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';

import {
  AdminAccess,
  EmployeeAccess,
  OrganizationAccess,
  RestaurantAccess,
  UserAccess,
} from './../decorators/guard.decorator';
import { validatePagination, validateSortOrder, validateStatus } from './../utils/functions';
import { SORT_ORDER, STATUSES } from './../utils/constants';

import { RestaurantMenuTypesService } from './restaurant-menu-types.service';
import { RestaurantMenuType } from './entities/restaurant-menu-type.entity';
// import { CreateRestaurantMenuTypeDto } from './dto/create-restaurant-menu-type.dto';
// import { UpdateRestaurantMenuTypeDto } from './dto/update-restaurant-menu-type.dto';

const prefix = 'restaurant-menu-types';

@ApiTags(prefix)
@ApiResponse({ status: 401, description: 'Unauthorized.' })
@UseInterceptors(ClassSerializerInterceptor)
@Controller(prefix)
export class RestaurantMenuTypesController {
  constructor(private readonly restaurantMenuTypesService: RestaurantMenuTypesService) {}

  // @Post()
  // create(@Body() createRestaurantMenuTypeDto: CreateRestaurantMenuTypeDto) {
  //   return this.restaurantMenuTypesService.create(createRestaurantMenuTypeDto);
  // }

  @AdminAccess()
  @RestaurantAccess()
  @OrganizationAccess()
  @EmployeeAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The records has been successfully fetched.' })
  @ApiQuery({
    name: 'status',
    required: false,
    type: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'page',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: `${Object.values(SORT_ORDER).filter((v) => isNaN(Number(v)))}`,
  })
  @Get()
  findAll(
    @Request() req,
    @Query('status') status: string | STATUSES,
    @Query() options: IPaginationOptions,
    @Query('sortOrder') sortOrder: SORT_ORDER,
  ): Promise<Pagination<RestaurantMenuType>> {
    validatePagination(options, prefix);
    sortOrder = validateSortOrder(sortOrder);
    status = validateStatus(status);

    return this.restaurantMenuTypesService.findAll(status, options, sortOrder);
  }

  @AdminAccess()
  @RestaurantAccess()
  @OrganizationAccess()
  @EmployeeAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.restaurantMenuTypesService.findOne(+id);
  }

  // @Patch(':id')
  // update(
  //   @Param('id') id: string,
  //   @Body() updateRestaurantMenuTypeDto: UpdateRestaurantMenuTypeDto,
  // ) {
  //   return this.restaurantMenuTypesService.update(+id, updateRestaurantMenuTypeDto);
  // }
}
