import { PartialType } from '@nestjs/swagger';

import { CreateRestaurantMenuTypeDto } from './create-restaurant-menu-type.dto';

export class UpdateRestaurantMenuTypeDto extends PartialType(CreateRestaurantMenuTypeDto) {
  id: number;
}
