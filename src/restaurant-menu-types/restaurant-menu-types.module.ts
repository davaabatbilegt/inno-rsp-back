import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { RestaurantMenuTypesService } from './restaurant-menu-types.service';
import { RestaurantMenuTypesController } from './restaurant-menu-types.controller';
import { RestaurantMenuType } from './entities/restaurant-menu-type.entity';

@Module({
  imports: [TypeOrmModule.forFeature([RestaurantMenuType])],
  controllers: [RestaurantMenuTypesController],
  providers: [RestaurantMenuTypesService],
  exports: [RestaurantMenuTypesService],
})
export class RestaurantMenuTypesModule {}
