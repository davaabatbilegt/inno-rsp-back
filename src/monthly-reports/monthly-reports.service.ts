import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, Repository } from 'typeorm';

import { MONTH_KEYS, SORT_ORDER, STATUSES, USER_TYPES } from './../utils/constants';
import {
  allUserTypeCondition,
  getMonthStartAndEnd,
  getSalaryMonthStartAndEnd,
} from './../utils/functions';

import { RestaurantOrderDetail } from './../restaurant-order-details/entities/restaurant-order-detail.entity';
import { Organization } from './../organizations/entities/organization.entity';

@Injectable()
export class MonthlyReportsService {
  constructor(
    @InjectRepository(RestaurantOrderDetail)
    private orderDetailRepository: Repository<RestaurantOrderDetail>,
    @InjectRepository(Organization)
    private organizationRepository: Repository<Organization>,
  ) {}

  async findAll(
    targetMonth: Date,
    restaurantId: number,
    restaurantMenuId: number,
    organizationId: number,
    sortOrder: SORT_ORDER,
    userId: string,
    type: string,
  ) {
    if (targetMonth == undefined) {
      throw new BadRequestException('targetMonth is not present!');
    }

    const monthMap = getMonthStartAndEnd(targetMonth);
    let condition = null;
    const queryBuilder = this.orderDetailRepository.createQueryBuilder('od');

    queryBuilder.andWhere({ status: Object.values(STATUSES)[STATUSES.ACTIVE] }).andWhere({
      orderDate: Between(monthMap.get(MONTH_KEYS.START), monthMap.get(MONTH_KEYS.END)),
    });

    condition = allUserTypeCondition(userId, type);

    if (condition) {
      queryBuilder.andWhere(condition);
    }

    if (restaurantId && type != USER_TYPES.RESTAURANT) {
      queryBuilder.andWhere({ restaurantId: restaurantId });
    }

    if (restaurantMenuId) {
      queryBuilder.andWhere({ restaurantMenuId: restaurantMenuId });
    }

    if (organizationId && type != USER_TYPES.ORGANIZATION) {
      queryBuilder.andWhere({ organizationId: organizationId });
    }

    queryBuilder
      .select('od.order_date')
      .leftJoin('od.restaurant', 'r')
      .addSelect('r.name')
      .leftJoin('od.organization', 'o')
      .addSelect('o.name')
      .leftJoin('od.menu', 'rm')
      .addSelect('rm.name')
      .addSelect('SUM(od.unit_count)', 'ordered')
      .addSelect('SUM(IF(od.order_status <> 0, od.unit_count, 0))', 'taken')
      .orderBy('od.order_date', sortOrder);

    if (type == USER_TYPES.ORGANIZATION) {
      queryBuilder.groupBy('od.order_date, od.restaurant_id, od.restaurant_menu_id');
    }

    if (type == USER_TYPES.RESTAURANT) {
      queryBuilder.groupBy('od.order_date, od.organization_id, od.restaurant_menu_id');
    }

    return queryBuilder.getRawMany();
  }

  async findAllEmployee(targetMonth: Date, sortOrder: SORT_ORDER, userId: string) {
    if (targetMonth == undefined) {
      throw new BadRequestException('targetMonth is not present!');
    }

    const organization = await this.organizationRepository.findOne({ where: { id: +userId } });
    const monthMap = getSalaryMonthStartAndEnd(targetMonth, organization.salaryMonthStartDay);
    const queryBuilder = this.orderDetailRepository.createQueryBuilder('od');

    queryBuilder.andWhere({ status: Object.values(STATUSES)[STATUSES.ACTIVE] }).andWhere({
      orderDate: Between(monthMap.get(MONTH_KEYS.START), monthMap.get(MONTH_KEYS.END)),
    });

    queryBuilder.andWhere({ organizationId: userId });

    queryBuilder
      .select('od.employee_id')
      .leftJoin('od.employee', 'e')
      .addSelect('e.employee_code')
      .addSelect('SUM(od.unit_count)', 'ordered')
      .addSelect('SUM(IF(od.order_status <> 0, od.unit_count, 0))', 'taken')
      .orderBy('e.employee_code', sortOrder)
      .groupBy('od.employee_id');

    return queryBuilder.getRawMany();
  }
}
