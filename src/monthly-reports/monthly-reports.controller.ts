import {
  Controller,
  Get,
  Request,
  UseInterceptors,
  ClassSerializerInterceptor,
  Query,
} from '@nestjs/common';
import { ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';

import { OrganizationAccess, RestaurantAccess } from './../decorators/guard.decorator';
import { validateSortOrder } from './../utils/functions';
import { SORT_ORDER } from './../utils/constants';

import { MonthlyReportsService } from './monthly-reports.service';

const prefix = 'monthly-reports';

@ApiTags(prefix)
@ApiResponse({ status: 401, description: 'Unauthorized.' })
@UseInterceptors(ClassSerializerInterceptor)
@Controller(prefix)
export class MonthlyReportsController {
  constructor(private readonly monthlyReportsService: MonthlyReportsService) {}

  @RestaurantAccess()
  @OrganizationAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @ApiQuery({
    name: 'targetMonth',
    required: true,
    type: Date,
  })
  @ApiQuery({
    name: 'restaurantId',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'restaurantMenuId',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'organizationId',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: `${Object.values(SORT_ORDER).filter((v) => isNaN(Number(v)))}`,
  })
  @Get()
  findAll(
    @Request() req,
    @Query('targetMonth') targetMonth: Date,
    @Query('restaurantId') restaurantId: number,
    @Query('restaurantMenuId') restaurantMenuId: number,
    @Query('organizationId') organizationId: number,
    @Query('sortOrder') sortOrder: SORT_ORDER,
  ) {
    sortOrder = validateSortOrder(sortOrder);

    return this.monthlyReportsService.findAll(
      targetMonth,
      restaurantId,
      restaurantMenuId,
      organizationId,
      sortOrder,
      req.user.id,
      req.user.type,
    );
  }

  @OrganizationAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @ApiQuery({
    name: 'targetMonth',
    required: true,
    type: Date,
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: `${Object.values(SORT_ORDER).filter((v) => isNaN(Number(v)))}`,
  })
  @Get('employee')
  findAllEmployee(
    @Request() req,
    @Query('targetMonth') targetMonth: Date,
    @Query('sortOrder') sortOrder: SORT_ORDER,
  ) {
    sortOrder = validateSortOrder(sortOrder);

    return this.monthlyReportsService.findAllEmployee(targetMonth, sortOrder, req.user.id);
  }
}
