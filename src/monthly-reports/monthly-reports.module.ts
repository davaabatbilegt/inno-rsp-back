import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { RestaurantOrderDetail } from './../restaurant-order-details/entities/restaurant-order-detail.entity';
import { Organization } from './../organizations/entities/organization.entity';

import { MonthlyReportsService } from './monthly-reports.service';
import { MonthlyReportsController } from './monthly-reports.controller';

@Module({
  imports: [TypeOrmModule.forFeature([RestaurantOrderDetail, Organization])],
  controllers: [MonthlyReportsController],
  providers: [MonthlyReportsService],
  exports: [MonthlyReportsService],
})
export class MonthlyReportsModule {}
