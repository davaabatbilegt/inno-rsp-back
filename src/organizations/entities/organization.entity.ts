import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Exclude, Expose } from 'class-transformer';

import { STATUSES } from './../../utils/constants';

import { OrganizationEmployee } from './../../organization-employees/entities/organization-employee.entity';
import { RestaurantOrder } from './../../restaurant-orders/entities/restaurant-order.entity';
import { RestaurantOrderDetail } from './../../restaurant-order-details/entities/restaurant-order-detail.entity';
import { RestaurantOrganizationContract } from './../../restaurant-organization-contracts/entities/restaurant-organization-contract.entity';

@Entity({ name: 'organizations' })
export class Organization {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column()
  name: string;

  @Column()
  address: string;

  @Column({ name: 'employee_count' })
  employeeCount: number;

  @Column()
  description: string;

  @Column({
    type: 'enum',
    enum: STATUSES,
    default: STATUSES.ACTIVE,
    transformer: {
      to: (value: STATUSES) => STATUSES[value],
      from: (value: number) => STATUSES[value],
    },
  })
  status: string;

  @Column({ unique: true })
  username: string;

  @Exclude()
  @Column()
  password: string;

  @Column({ name: 'salary_month_start_day' })
  salaryMonthStartDay: number;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @Expose()
  contracted?: number;

  @OneToMany(() => OrganizationEmployee, (employee) => employee.organization)
  employees: OrganizationEmployee[];

  @OneToMany(() => RestaurantOrganizationContract, (contract) => contract.organization)
  contracts: RestaurantOrganizationContract[];

  @OneToMany(() => RestaurantOrder, (order) => order.organization)
  orders: RestaurantOrder[];

  @OneToMany(() => RestaurantOrderDetail, (orderDetail) => orderDetail.organization)
  orderDetails: RestaurantOrderDetail[];

  constructor(partial: Partial<Organization>) {
    Object.assign(this, partial);
  }
}
