import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Param,
  Patch,
  Query,
  Request,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';

import { AdminAccess, OrganizationAccess, RestaurantAccess } from './../decorators/guard.decorator';
import { validatePagination, validateSortOrder, validateStatus } from './../utils/functions';
import { SORT_ORDER, STATUSES } from './../utils/constants';

import { OrganizationsService } from './organizations.service';
// import { CreateOrganizationDto } from './dto/create-organization.dto';
import { UpdateOrganizationDto } from './dto/update-organization.dto';
import { Organization } from './entities/organization.entity';

const prefix = 'organizations';

@ApiTags(prefix)
@ApiResponse({ status: 401, description: 'Unauthorized.' })
@UseInterceptors(ClassSerializerInterceptor)
@Controller(prefix)
export class OrganizationsController {
  constructor(private readonly organizationsService: OrganizationsService) {}

  // @Post()
  // create(@Body() createOrganizationDto: CreateOrganizationDto) {
  //   return this.organizationsService.create(createOrganizationDto);
  // }

  @AdminAccess()
  @RestaurantAccess()
  @OrganizationAccess()
  @ApiResponse({ status: 200, description: 'The records has been successfully fetched.' })
  @ApiQuery({
    name: 'status',
    required: false,
    type: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'contracted',
    required: false,
    type: Boolean,
  })
  @ApiQuery({
    name: 'page',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: `${Object.values(SORT_ORDER).filter((v) => isNaN(Number(v)))}`,
  })
  @Get()
  findAll(
    @Request() req,
    @Query('status') status: string | STATUSES,
    @Query('contracted') contracted: boolean,
    @Query() options: IPaginationOptions,
    @Query('sortOrder') sortOrder: SORT_ORDER,
  ): Promise<Pagination<Organization>> {
    validatePagination(options, prefix);
    sortOrder = validateSortOrder(sortOrder);
    status = validateStatus(status);

    return this.organizationsService.findAll(
      status,
      contracted,
      options,
      sortOrder,
      req.user.id,
      req.user.type,
    );
  }

  @AdminAccess()
  @RestaurantAccess()
  @OrganizationAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @Get(':id')
  findOne(@Request() req, @Param('id') id: string) {
    return this.organizationsService.findOne(+id, req.user.id, req.user.type);
  }

  @OrganizationAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully updated.' })
  @ApiBody({
    type: UpdateOrganizationDto,
    description: 'Json structure for organization object',
  })
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateOrganizationDto: UpdateOrganizationDto) {
    return this.organizationsService.update(+id, updateOrganizationDto);
  }
}
