import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { IPaginationOptions, Pagination, paginate, paginateRaw } from 'nestjs-typeorm-paginate';

import { SORT_ORDER, STATUSES, USER_TYPES } from './../utils/constants';
import { passwordMd5Encrypt } from './../utils/functions';

// import { CreateOrganizationDto } from './dto/create-organization.dto';
import { UpdateOrganizationDto } from './dto/update-organization.dto';
import { Organization } from './entities/organization.entity';

@Injectable()
export class OrganizationsService {
  constructor(
    @InjectRepository(Organization)
    private organizationRepository: Repository<Organization>,
  ) {}

  async findAll(
    status: string | STATUSES,
    contracted: boolean,
    options: IPaginationOptions,
    sortOrder: SORT_ORDER,
    userId: string,
    type: string,
  ): Promise<Pagination<Organization>> {
    const queryBuilder = this.organizationRepository.createQueryBuilder('o');

    if (status) {
      queryBuilder.andWhere({ status: status });
    }

    queryBuilder.orderBy('o.name', sortOrder).getRawMany();

    if (type == USER_TYPES.RESTAURANT) {
      queryBuilder
        .leftJoin('o.contracts', 'roc')
        .addSelect(
          `SUM(IF(roc.restaurant_id = ${userId} AND roc.status = 1 AND roc.restaurant_status = 1 AND roc.organization_status = 1, 1, 0))`,
          'contracted',
        )
        .groupBy('o.id');

      if (contracted != undefined) {
        if (contracted) {
          queryBuilder.having('contracted > 0');
        } else {
          queryBuilder.having('contracted = 0');
        }
      }
      const raw = await paginateRaw<Organization>(queryBuilder, options);
      const entities = await paginate<Organization>(queryBuilder, options);

      for (let i = 0; i < entities.items.length; i++) {
        entities.items[i].contracted = raw.items[i].contracted;
      }
      return entities;
    } else {
      return paginate<Organization>(queryBuilder, options);
    }
  }

  async findOne(id: number, userId: string, type: string): Promise<Organization> {
    const queryBuilder = this.organizationRepository.createQueryBuilder('o');

    if (type == USER_TYPES.RESTAURANT) {
      queryBuilder
        .leftJoin('o.contracts', 'roc')
        .addSelect(
          `SUM(IF(roc.restaurant_id = ${userId} AND roc.status = 1 AND roc.restaurant_status = 1 AND roc.organization_status = 1, 1, 0))`,
          'contracted',
        )
        .andWhere({ id: id });
    } else {
      queryBuilder.andWhere({ id: id });
    }

    const organization = await queryBuilder.getOne();

    if (!organization) throw new UnauthorizedException('error.organization not found');

    if (type == USER_TYPES.RESTAURANT) {
      const organizationRaw = await queryBuilder.getRawOne();
      organization.contracted = organizationRaw['contracted'];
    }

    return organization;
  }

  async findOneByUsername(username: string, statuses?: STATUSES[]): Promise<Organization> {
    let condition = null;

    if (statuses) {
      condition = { status: In(statuses) };
    } else {
      condition = { status: Object.values(STATUSES)[STATUSES.ACTIVE] };
    }

    const organization = await this.organizationRepository.findOne({
      where: {
        username: username,
        ...condition,
      },
    });

    if (!organization) throw new UnauthorizedException('error.organization not found');

    return organization;
  }

  async findOneById(userId: string, statuses?: STATUSES[]): Promise<Organization> {
    let condition = null;

    if (statuses) {
      condition = { status: In(statuses) };
    } else {
      condition = { status: Object.values(STATUSES)[STATUSES.ACTIVE] };
    }

    const organization = await this.organizationRepository.findOne({
      where: {
        id: userId,
        ...condition,
      },
    });

    if (!organization) throw new UnauthorizedException('error.organization not found');

    return organization;
  }

  // create(createOrganizationDto: CreateOrganizationDto) {
  //   console.log(createOrganizationDto);
  //   return 'This action adds a new organization';
  // }

  async update(id: number, updateOrganizationDto: UpdateOrganizationDto) {
    if (!id) throw new BadRequestException(`Update error: id is empty.`);

    try {
      updateOrganizationDto.id = id;

      if (updateOrganizationDto.password) {
        updateOrganizationDto.password = await passwordMd5Encrypt(updateOrganizationDto.password);
      }

      return this.organizationRepository.save(updateOrganizationDto);
    } catch (ex) {
      throw new BadRequestException(`Update error: ${ex.message}`);
    }
  }
}
