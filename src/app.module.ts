import { Logger, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { HeaderResolver, I18nModule } from 'nestjs-i18n';
import { join } from 'path';
import { DataSource } from 'typeorm';

import databaseConfig from './config/database-factory';
import { validate } from './../env.validation';

import { AuthModule } from './auth/auth.module';
import { AdminsModule } from './admins/admins.module';
import { OrganizationEmployeesModule } from './organization-employees/organization-employees.module';
import { OrganizationsModule } from './organizations/organizations.module';
import { RestaurantsModule } from './restaurants/restaurants.module';
import { RestaurantInvoicesModule } from './restaurant-invoices/restaurant-invoices.module';
import { RestaurantMenusModule } from './restaurant-menus/restaurant-menus.module';
import { RestaurantMenuTypesModule } from './restaurant-menu-types/restaurant-menu-types.module';
import { RestaurantOrderDetailsModule } from './restaurant-order-details/restaurant-order-details.module';
import { RestaurantOrdersModule } from './restaurant-orders/restaurant-orders.module';
import { RestaurantOrganizationContractsModule } from './restaurant-organization-contracts/restaurant-organization-contracts.module';
import { RestaurantTablesModule } from './restaurant-tables/restaurant-tables.module';
import { UsersModule } from './users/users.module';
import { RestaurantInvoiceDetailsModule } from './restaurant-invoice-details/restaurant-invoice-details.module';
import { MonthlyReportsModule } from './monthly-reports/monthly-reports.module';
import { QrCodesModule } from './qr-codes/qr-codes.module';

const typeOrmConfig = {
  imports: [
    ConfigModule.forRoot({
      load: [databaseConfig],
    }),
  ],
  inject: [ConfigService],
  useFactory: async (configService: ConfigService) => configService.get('database'),
  dataSourceFactory: async (options) => new DataSource(options).initialize(),
};

@Module({
  imports: [
    ConfigModule.forRoot({ validate }),
    TypeOrmModule.forRootAsync(typeOrmConfig),
    I18nModule.forRoot({
      fallbackLanguage: 'en',
      loaderOptions: {
        path: join(__dirname, '/i18n/'),
        watch: true,
      },
      resolvers: [new HeaderResolver(['x-lang'])],
    }),
    AuthModule,
    AdminsModule,
    OrganizationEmployeesModule,
    OrganizationsModule,
    RestaurantsModule,
    RestaurantInvoicesModule,
    RestaurantMenusModule,
    RestaurantMenuTypesModule,
    RestaurantOrderDetailsModule,
    RestaurantOrdersModule,
    RestaurantOrganizationContractsModule,
    RestaurantTablesModule,
    UsersModule,
    RestaurantInvoiceDetailsModule,
    MonthlyReportsModule,
    QrCodesModule,
  ],
  controllers: [],
  providers: [Logger],
})
export class AppModule {}
