import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';

import { STATUSES } from './../../utils/constants';

import { OrganizationEmployee } from './../../organization-employees/entities/organization-employee.entity';
import { RestaurantOrder } from './../../restaurant-orders/entities/restaurant-order.entity';
import { RestaurantOrderDetail } from './../../restaurant-order-details/entities/restaurant-order-detail.entity';

@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column()
  name: string;

  @Column({
    type: 'enum',
    enum: STATUSES,
    default: STATUSES.ACTIVE,
    transformer: {
      to: (value: STATUSES) => STATUSES[value],
      from: (value: number) => STATUSES[value],
    },
  })
  status: string;

  @Column({ unique: true })
  username: string;

  @Exclude()
  @Column()
  password: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @OneToMany(() => OrganizationEmployee, (employee) => employee.organization)
  employees: OrganizationEmployee[];

  @OneToMany(() => RestaurantOrder, (order) => order.user)
  orders: RestaurantOrder[];

  @OneToMany(() => RestaurantOrderDetail, (orderDetail) => orderDetail.employee)
  orderDetails: RestaurantOrderDetail[];

  constructor(partial: Partial<User>) {
    Object.assign(this, partial);
  }
}
