import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { IPaginationOptions, Pagination, paginate } from 'nestjs-typeorm-paginate';

import { STATUSES, SORT_ORDER } from '../utils/constants';

// import { CreateUserDto } from './dto/create-user.dto';
// import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(@InjectRepository(User) private userRepository: Repository<User>) {}

  async findAll(
    status: string | STATUSES,
    options: IPaginationOptions,
    sortOrder: SORT_ORDER,
  ): Promise<Pagination<User>> {
    const queryBuilder = this.userRepository.createQueryBuilder('u');

    if (status) {
      queryBuilder.andWhere({ status: status });
    }

    queryBuilder.orderBy('u.name', sortOrder);

    return paginate<User>(queryBuilder, options);
  }

  async findOneByUsername(username: string, statuses?: STATUSES[]): Promise<User> {
    let condition = null;

    if (statuses) {
      condition = { status: In(statuses) };
    } else {
      condition = { status: Object.values(STATUSES)[STATUSES.ACTIVE] };
    }

    const user = await this.userRepository.findOne({
      where: {
        username: username,
        ...condition,
      },
    });

    if (!user) throw new UnauthorizedException('error.user not found');

    return user;
  }

  async findOneById(userId: string, statuses?: STATUSES[]): Promise<User> {
    let condition = null;

    if (statuses) {
      condition = { status: In(statuses) };
    } else {
      condition = { status: Object.values(STATUSES)[STATUSES.ACTIVE] };
    }

    const user = await this.userRepository.findOne({
      where: {
        id: userId,
        ...condition,
      },
    });

    if (!user) throw new UnauthorizedException('error.user not found');

    return user;
  }

  // create(createUserDto: CreateUserDto) {
  //   console.log(createUserDto);
  //   return 'This action adds a new user';
  // }

  // update(id: number, updateUserDto: UpdateUserDto) {
  //   console.log(updateUserDto);
  //   return `This action updates a #${id} user`;
  // }
}
