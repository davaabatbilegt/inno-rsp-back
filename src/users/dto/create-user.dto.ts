import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, Length } from 'class-validator';

import { STATUSES } from './../../utils/constants';

export class CreateUserDto {
  @ApiProperty({
    example: 'testUser',
    required: true,
  })
  @IsNotEmpty({ message: 'Please enter user name' })
  name: string;

  @ApiProperty({
    enum: STATUSES,
    example: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
    required: false,
  })
  status: string;

  @ApiProperty({
    example: 'testUser',
    required: true,
  })
  @IsNotEmpty({ message: 'Please enter username' })
  username: string;

  @ApiProperty({
    example: 'myPowefulPa$$001',
    required: true,
  })
  @IsNotEmpty({ message: 'Please enter password' })
  @Length(6, 50, { message: 'Password length Must be between 6 and 50 charcters' })
  password: string;
}
