import {
  Controller,
  Get,
  Param,
  Request,
  UseInterceptors,
  ClassSerializerInterceptor,
  Query,
} from '@nestjs/common';
import { ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';

import {
  AdminAccess,
  EmployeeAccess,
  OrganizationAccess,
  RestaurantAccess,
  UserAccess,
} from './../decorators/guard.decorator';
import { validatePagination, validateSortOrder } from './../utils/functions';
import { SORT_ORDER, STATUSES } from './../utils/constants';

import { UsersService } from './users.service';
// import { CreateUserDto } from './dto/create-user.dto';
// import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

const prefix = 'users';

@ApiTags(prefix)
@ApiResponse({ status: 401, description: 'Unauthorized.' })
@UseInterceptors(ClassSerializerInterceptor)
@Controller(prefix)
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  // @Post()
  // create(@Body() createUserDto: CreateUserDto) {
  //   return this.usersService.create(createUserDto);
  // }

  @AdminAccess()
  @EmployeeAccess()
  @RestaurantAccess()
  @OrganizationAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @ApiQuery({
    name: 'status',
    required: false,
    type: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'page',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: `${Object.values(SORT_ORDER).filter((v) => isNaN(Number(v)))}`,
  })
  @Get()
  findAll(
    @Request() req,
    @Query('status') status: string | STATUSES,
    @Query() options: IPaginationOptions,
    @Query('sortOrder') sortOrder: SORT_ORDER,
  ): Promise<Pagination<User>> {
    validatePagination(options, prefix);
    sortOrder = validateSortOrder(sortOrder);

    return this.usersService.findAll(status, options, sortOrder);
  }

  @AdminAccess()
  @EmployeeAccess()
  @RestaurantAccess()
  @OrganizationAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @Get(':id')
  findOne(@Request() req, @Param('id') id: string) {
    return this.usersService.findOneById(id);
  }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
  //   return this.usersService.update(+id, updateUserDto);
  // }
}
