import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MoreThan, Not, Repository } from 'typeorm';
import { IPaginationOptions, Pagination, paginate } from 'nestjs-typeorm-paginate';
import * as moment from 'moment';

import {
  ORDER_IN_CONTRACTS,
  ORDER_STATUS,
  SORT_ORDER,
  STATUSES,
  USER_TYPES,
} from './../utils/constants';
import { allUserTypeCondition } from './../utils/functions';

import { CreateRestaurantOrderDto } from './dto/create-restaurant-order.dto';
import { UpdateRestaurantOrderDto } from './dto/update-restaurant-order.dto';
import { RestaurantOrder } from './entities/restaurant-order.entity';

import { CreateRestaurantOrderDetailDto } from './../restaurant-order-details/dto/create-restaurant-order-detail.dto';
import { OrganizationEmployee } from './../organization-employees/entities/organization-employee.entity';

@Injectable()
export class RestaurantOrdersService {
  constructor(
    @InjectRepository(OrganizationEmployee)
    private employeeRepository: Repository<OrganizationEmployee>,
    @InjectRepository(RestaurantOrder)
    private orderRepository: Repository<RestaurantOrder>,
  ) {}

  async create(
    createRestaurantOrderDto: CreateRestaurantOrderDto,
    userId: string,
    type: string,
  ): Promise<RestaurantOrder | undefined> {
    try {
      if (type == USER_TYPES.EMPLOYEE) {
        createRestaurantOrderDto.employeeId = parseInt(userId, 10);
      } else if (type == USER_TYPES.USER) {
        createRestaurantOrderDto.userId = parseInt(userId, 10);
      } else {
        throw new BadRequestException(`Only emloyee or user can create order`);
      }
      return this.orderRepository.save(createRestaurantOrderDto);
    } catch (ex) {
      throw new BadRequestException(`Create error: ${ex.message}`);
    }
  }

  async getActiveOrder(
    orderDetail: CreateRestaurantOrderDetailDto,
    userId: string,
    type: string,
  ): Promise<RestaurantOrder> {
    let condition = null;

    condition = allUserTypeCondition(userId, type);
    const start = moment().startOf('day').toDate();

    // search target restaurant, target user, created today and not billed order
    let order = await this.orderRepository.findOne({
      where: {
        restaurantId: orderDetail.restaurantId,
        isInsideContractRange: orderDetail.isInsideContractRange,
        status: Object.values(STATUSES)[STATUSES.ACTIVE],
        orderStatus: Not(Object.values(ORDER_STATUS)[ORDER_STATUS.BILLED]),
        createdAt: MoreThan(start),
        ...condition,
      },
    });

    // if order not found create one
    if (order == null) {
      const createRestaurantOrderDto = new CreateRestaurantOrderDto();
      createRestaurantOrderDto.employeeId = orderDetail.employeeId;
      createRestaurantOrderDto.userId = orderDetail.userId;
      createRestaurantOrderDto.organizationId = orderDetail.organizationId;
      createRestaurantOrderDto.restaurantId = orderDetail.restaurantId;
      createRestaurantOrderDto.isInsideContractRange = orderDetail.isInsideContractRange;
      createRestaurantOrderDto.totalBill = orderDetail.totalPrice;
      order = await this.create(createRestaurantOrderDto, userId, type);
    } else {
      order.totalBill = order.totalBill + orderDetail.totalPrice;
      this.orderRepository.save(order);
    }

    return order;
  }

  async findAll(
    restaurantId: number,
    organizationId: number,
    employeeId: number,
    isInsideContractRange: string | ORDER_IN_CONTRACTS,
    orderDate: Date,
    orderStatus: string | ORDER_STATUS,
    status: string | STATUSES,
    options: IPaginationOptions,
    sortOrder: SORT_ORDER,
    userId: string,
    type: string,
  ): Promise<Pagination<RestaurantOrder>> {
    const queryBuilder = this.orderRepository.createQueryBuilder('ro');

    if (type == USER_TYPES.RESTAURANT) {
      queryBuilder.andWhere({ restaurantId: userId });
    } else if (restaurantId) {
      queryBuilder.andWhere({ restaurantId: restaurantId });
    }

    if (type == USER_TYPES.ORGANIZATION) {
      queryBuilder.andWhere({ organizationId: userId });
    } else if (organizationId) {
      queryBuilder.andWhere({ organizationId: organizationId });
    }

    if (type == USER_TYPES.EMPLOYEE && isNaN(organizationId)) {
      const employee = await this.employeeRepository.findOne({
        where: {
          id: +userId,
        },
      });

      if (employee) {
        queryBuilder.andWhere({ organizationId: employee.organizationId });
      }
    }

    if (isInsideContractRange) {
      queryBuilder.andWhere({ isInsideContractRange: isInsideContractRange });
    }

    if (orderDate) {
      queryBuilder.andWhere({ orderDate: orderDate });
    }

    if (orderStatus) {
      queryBuilder.andWhere({ orderStatus: orderStatus });
    }

    if (status) {
      queryBuilder.andWhere({ status: status });
    }

    queryBuilder
      .leftJoinAndSelect('ro.employee', 'e')
      .leftJoinAndSelect('ro.restaurant', 'r')
      .leftJoinAndSelect('ro.organization', 'o')
      .leftJoinAndSelect('ro.orderDetails', 'details', 'details.status = :status', {
        status: STATUSES.ACTIVE,
      })
      .leftJoinAndSelect('details.menu', 'menu')
      .orderBy('ro.createdAt', sortOrder);

    return paginate<RestaurantOrder>(queryBuilder, options);
  }

  async findOne(id: number, userId: string, type: string): Promise<RestaurantOrder> {
    let condition = null;
    condition = allUserTypeCondition(userId, type);

    const order = await this.orderRepository.findOne({
      where: {
        id: id,
        ...condition,
      },
    });

    if (!order) throw new UnauthorizedException('error.order not found');

    return order;
  }

  update(
    id: number,
    updateRestaurantOrderDto: UpdateRestaurantOrderDto,
  ): Promise<RestaurantOrder | undefined> {
    if (!id) throw new BadRequestException(`Update error: id is empty.`);

    try {
      updateRestaurantOrderDto.id = id;

      return this.orderRepository.save(updateRestaurantOrderDto);
    } catch (ex) {
      throw new BadRequestException(`Update error: ${ex.message}`);
    }
  }
}
