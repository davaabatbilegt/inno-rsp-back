import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { RestaurantOrdersService } from './restaurant-orders.service';
import { RestaurantOrdersController } from './restaurant-orders.controller';
import { RestaurantOrder } from './entities/restaurant-order.entity';

import { OrganizationEmployee } from './../organization-employees/entities/organization-employee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([OrganizationEmployee, RestaurantOrder])],
  controllers: [RestaurantOrdersController],
  providers: [RestaurantOrdersService],
  exports: [RestaurantOrdersService],
})
export class RestaurantOrdersModule {}
