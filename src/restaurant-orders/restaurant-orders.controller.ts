import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Request,
  UseInterceptors,
  ClassSerializerInterceptor,
  Query,
} from '@nestjs/common';
import { ApiBody, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';

import {
  EmployeeAccess,
  OrganizationAccess,
  RestaurantAccess,
  UserAccess,
} from './../decorators/guard.decorator';
import {
  validateIsInsideContractRange,
  validateOrderStatus,
  validatePagination,
  validateSortOrder,
  validateStatus,
} from './../utils/functions';
import { ORDER_IN_CONTRACTS, ORDER_STATUS, SORT_ORDER, STATUSES } from './../utils/constants';

import { RestaurantOrdersService } from './restaurant-orders.service';
import { CreateRestaurantOrderDto } from './dto/create-restaurant-order.dto';
import { UpdateRestaurantOrderDto } from './dto/update-restaurant-order.dto';
import { RestaurantOrder } from './entities/restaurant-order.entity';

const prefix = 'restaurant-orders';

@ApiTags(prefix)
@ApiResponse({ status: 401, description: 'Unauthorized.' })
@UseInterceptors(ClassSerializerInterceptor)
@Controller(prefix)
export class RestaurantOrdersController {
  constructor(private readonly restaurantOrdersService: RestaurantOrdersService) {}

  @EmployeeAccess()
  @UserAccess()
  @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
  @ApiBody({
    type: CreateRestaurantOrderDto,
    description: 'Json structure for restaurant order object',
  })
  @Post()
  create(@Request() req, @Body() createRestaurantOrderDto: CreateRestaurantOrderDto) {
    return this.restaurantOrdersService.create(
      createRestaurantOrderDto,
      req.user.id,
      req.user.type,
    );
  }

  @RestaurantAccess()
  @OrganizationAccess()
  @EmployeeAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @ApiQuery({
    name: 'restaurantId',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'restaurantMenuId',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'organizationId',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'employeeId',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'isInsideContractRange',
    required: false,
    type: `${Object.values(ORDER_IN_CONTRACTS).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'orderDate',
    required: false,
    type: Date,
  })
  @ApiQuery({
    name: 'orderStatus',
    required: false,
    type: `${Object.values(ORDER_STATUS).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'status',
    required: false,
    type: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'page',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: `${Object.values(SORT_ORDER).filter((v) => isNaN(Number(v)))}`,
  })
  @Get()
  findAll(
    @Request() req,
    @Query('restaurantId') restaurantId: number,
    @Query('organizationId') organizationId: number,
    @Query('employeeId') employeeId: number,
    @Query('isInsideContractRange') isInsideContractRange: string | ORDER_IN_CONTRACTS,
    @Query('orderDate') orderDate: Date,
    @Query('orderStatus') orderStatus: string | ORDER_STATUS,
    @Query('status') status: string | STATUSES,
    @Query() options: IPaginationOptions,
    @Query('sortOrder') sortOrder: SORT_ORDER,
  ): Promise<Pagination<RestaurantOrder>> {
    validatePagination(options, prefix);
    sortOrder = validateSortOrder(sortOrder);
    orderStatus = validateOrderStatus(orderStatus);
    isInsideContractRange = validateIsInsideContractRange(isInsideContractRange);
    status = validateStatus(status);

    return this.restaurantOrdersService.findAll(
      restaurantId,
      organizationId,
      employeeId,
      isInsideContractRange,
      orderDate,
      orderStatus,
      status,
      options,
      sortOrder,
      req.user.id,
      req.user.type,
    );
  }

  @RestaurantAccess()
  @OrganizationAccess()
  @EmployeeAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @Get(':id')
  findOne(@Request() req, @Param('id') id: string) {
    return this.restaurantOrdersService.findOne(+id, req.user.id, req.user.type);
  }

  @RestaurantAccess()
  @OrganizationAccess()
  @EmployeeAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully updated.' })
  @ApiBody({
    type: UpdateRestaurantOrderDto,
    description: 'Json structure for restaurant order object',
  })
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateRestaurantOrderDto: UpdateRestaurantOrderDto) {
    return this.restaurantOrdersService.update(+id, updateRestaurantOrderDto);
  }
}
