import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { ORDER_IN_CONTRACTS, ORDER_STATUS, STATUSES } from './../../utils/constants';

import { Restaurant } from './../../restaurants/entities/restaurant.entity';
import { RestaurantOrderDetail } from './../../restaurant-order-details/entities/restaurant-order-detail.entity';
import { RestaurantTable } from './../../restaurant-tables/entities/restaurant-table.entity';
import { Organization } from './../../organizations/entities/organization.entity';
import { OrganizationEmployee } from './../../organization-employees/entities/organization-employee.entity';
import { User } from './../../users/entities/user.entity';

@Entity({ name: 'restaurant_orders' })
export class RestaurantOrder {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ name: 'restaurant_id' })
  restaurantId: number;

  @Column({ name: 'table_id' })
  tableId: number;

  @Column({ name: 'organization_id' })
  organizationId: number;

  @Column({ name: 'employee_id' })
  employeeId: number;

  @Column({ name: 'user_id' })
  userId: number;

  @Column({
    name: 'is_inside_contract_range',
    type: 'enum',
    enum: ORDER_IN_CONTRACTS,
    default: ORDER_IN_CONTRACTS.NON_CONTRACT,
    transformer: {
      to: (value: ORDER_IN_CONTRACTS) => ORDER_IN_CONTRACTS[value],
      from: (value: number) => ORDER_IN_CONTRACTS[value],
    },
  })
  isInsideContractRange: string;

  @Column({ name: 'order_date' })
  orderDate: Date;

  @Column({
    name: 'order_status',
    type: 'enum',
    enum: ORDER_STATUS,
    default: ORDER_STATUS.ORDERED,
    transformer: {
      to: (value: ORDER_STATUS) => ORDER_STATUS[value],
      from: (value: number) => ORDER_STATUS[value],
    },
  })
  orderStatus: string;

  @Column({
    type: 'enum',
    enum: STATUSES,
    default: STATUSES.ACTIVE,
    transformer: {
      to: (value: STATUSES) => STATUSES[value],
      from: (value: number) => STATUSES[value],
    },
  })
  status: string;

  @Column({ name: 'total_bill' })
  totalBill: number;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  // Many to One cascade
  @ManyToOne(() => Organization, (organization) => organization.orders, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: 'organization_id' })
  organization: Organization;

  @ManyToOne(() => OrganizationEmployee, (employee) => employee.orders, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: 'employee_id' })
  employee: OrganizationEmployee;

  @ManyToOne(() => Restaurant, (restaurant) => restaurant.orders, { cascade: true, eager: true })
  @JoinColumn({ name: 'restaurant_id' })
  restaurant: Restaurant;

  @ManyToOne(() => RestaurantTable, (table) => table.orders, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: 'table_id' })
  table: RestaurantTable;

  @ManyToOne(() => User, (user) => user.orders, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: 'user_id' })
  user: User;

  // One to Many cascade
  @OneToMany(() => RestaurantOrderDetail, (orderDetail) => orderDetail.order)
  orderDetails: RestaurantOrderDetail[];

  constructor(partial: Partial<RestaurantOrder>) {
    Object.assign(this, partial);
  }
}
