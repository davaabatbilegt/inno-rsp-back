import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';

import {
  EmployeeAccess,
  OrganizationAccess,
  RestaurantAccess,
  UserAccess,
} from './../decorators/guard.decorator';

import { QrCodesService } from './qr-codes.service';
import { json } from 'stream/consumers';

const prefix = 'qr-codes';

@ApiTags(prefix)
@ApiResponse({ status: 401, description: 'Unauthorized.' })
@UseInterceptors(ClassSerializerInterceptor)
@Controller(prefix)
export class QrCodesController {
  constructor(private readonly qrCodesService: QrCodesService) {}

  @RestaurantAccess()
  @OrganizationAccess()
  @EmployeeAccess()
  @UserAccess()
  @ApiResponse({ status: 201, description: 'QR code successfully encrypted.' })
  @ApiBody({
    type: json,
    description: 'Json structure for QR code',
  })
  @Post('encrypt')
  encrypt(@Body() textToEncrypt: string) {
    return this.qrCodesService.encrypt(textToEncrypt);
  }

  @RestaurantAccess()
  @OrganizationAccess()
  @EmployeeAccess()
  @UserAccess()
  @ApiResponse({ status: 200, description: 'QR code successfully decrypted.' })
  @ApiQuery({
    name: 'encryptedText',
    required: false,
    type: Number,
  })
  @Get('decrypt')
  decrypt(@Query('encryptedText') encryptedText: string) {
    return this.qrCodesService.decrypt(encryptedText);
  }
}
