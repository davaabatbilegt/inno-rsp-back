import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { createCipher, createDecipher } from 'crypto';

@Injectable()
export class QrCodesService {
  constructor(private configService: ConfigService) {}

  async encrypt(textToEncrypt: string) {
    const key = await this.configService.get<string>('QR_SECRET');

    const cipher = createCipher('aes-256-cbc', key);

    const encryptedTextUpdate = cipher.update(JSON.stringify(textToEncrypt), 'utf8', 'hex');
    const encryptedText = encryptedTextUpdate + cipher.final('hex');

    return encryptedText;
  }

  async decrypt(encryptedText: string) {
    const key = await this.configService.get<string>('QR_SECRET');

    const decipher = createDecipher('aes-256-cbc', key);

    const decryptedTextUpdate = decipher.update(encryptedText, 'hex', 'utf8');
    const decryptedText = decryptedTextUpdate + decipher.final('utf8');

    const jsonText = JSON.parse(decryptedText);

    return jsonText;
  }
}
