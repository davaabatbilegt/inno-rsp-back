import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Request,
  UseInterceptors,
  ClassSerializerInterceptor,
  Query,
} from '@nestjs/common';
import { ApiBody, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';

import { OrganizationAccess, RestaurantAccess } from './../decorators/guard.decorator';
import { validatePagination, validateSortOrder, validateStatus } from './../utils/functions';
import { SORT_ORDER, STATUSES } from './../utils/constants';

import { OrganizationEmployeesService } from './organization-employees.service';
import { CreateOrganizationEmployeeDto } from './dto/create-organization-employee.dto';
import { UpdateOrganizationEmployeeDto } from './dto/update-organization-employee.dto';
import { OrganizationEmployee } from './entities/organization-employee.entity';

const prefix = 'organization-employees';

@ApiTags(prefix)
@ApiResponse({ status: 401, description: 'Unauthorized.' })
@UseInterceptors(ClassSerializerInterceptor)
@Controller(prefix)
export class OrganizationEmployeesController {
  constructor(private readonly organizationEmployeesService: OrganizationEmployeesService) {}

  @OrganizationAccess()
  @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
  @ApiBody({
    type: CreateOrganizationEmployeeDto,
    description: 'Json structure for organization employee object',
  })
  @Post()
  create(@Request() req, @Body() createOrganizationEmployeeDto: CreateOrganizationEmployeeDto) {
    return this.organizationEmployeesService.create(
      createOrganizationEmployeeDto,
      req.user.id,
      req.user.type,
    );
  }

  @RestaurantAccess()
  @OrganizationAccess()
  @ApiResponse({ status: 200, description: 'The records has been successfully fetched.' })
  @ApiQuery({
    name: 'status',
    required: false,
    type: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
  })
  @ApiQuery({
    name: 'page',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: `${Object.values(SORT_ORDER).filter((v) => isNaN(Number(v)))}`,
  })
  @Get()
  findAll(
    @Request() req,
    @Query('status') status: string | STATUSES,
    @Query() options: IPaginationOptions,
    @Query('sortOrder') sortOrder: SORT_ORDER,
  ): Promise<Pagination<OrganizationEmployee>> {
    validatePagination(options, prefix);
    sortOrder = validateSortOrder(sortOrder);
    status = validateStatus(status);

    return this.organizationEmployeesService.findAll(
      status,
      options,
      sortOrder,
      req.user.id,
      req.user.type,
    );
  }

  @RestaurantAccess()
  @OrganizationAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully fetched.' })
  @Get(':id')
  findOne(@Request() req, @Param('id') id: string) {
    return this.organizationEmployeesService.findOne(+id, req.user.id, req.user.type);
  }

  @OrganizationAccess()
  @ApiResponse({ status: 200, description: 'The record has been successfully updated.' })
  @ApiBody({
    type: UpdateOrganizationEmployeeDto,
    description: 'Json structure for organization employee object',
  })
  @Patch(':id')
  update(
    @Request() req,
    @Param('id') id: string,
    @Body() updateOrganizationEmployeeDto: UpdateOrganizationEmployeeDto,
  ) {
    return this.organizationEmployeesService.update(
      +id,
      updateOrganizationEmployeeDto,
      req.user.id,
      req.user.type,
    );
  }
}
