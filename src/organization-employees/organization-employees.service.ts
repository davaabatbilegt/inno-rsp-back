import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Not, Repository } from 'typeorm';
import { IPaginationOptions, Pagination, paginate } from 'nestjs-typeorm-paginate';

import { SORT_ORDER, STATUSES, USER_TYPES } from './../utils/constants';
import { organizationCondition, passwordMd5Encrypt } from './../utils/functions';

import { CreateOrganizationEmployeeDto } from './dto/create-organization-employee.dto';
import { UpdateOrganizationEmployeeDto } from './dto/update-organization-employee.dto';
import { OrganizationEmployee } from './entities/organization-employee.entity';

@Injectable()
export class OrganizationEmployeesService {
  constructor(
    @InjectRepository(OrganizationEmployee)
    private employeeRepository: Repository<OrganizationEmployee>,
  ) {}

  async create(
    createOrganizationEmployeeDto: CreateOrganizationEmployeeDto,
    userId: string,
    type: string,
  ): Promise<OrganizationEmployee | undefined> {
    try {
      if (type == USER_TYPES.ORGANIZATION) {
        createOrganizationEmployeeDto.organizationId = parseInt(userId, 10);
      }
      const isUnique = await this.isUsernameUnique(createOrganizationEmployeeDto.username);
      if (isUnique == false) {
        throw new BadRequestException('Username is duplicated');
      }
      createOrganizationEmployeeDto.password = await passwordMd5Encrypt(
        createOrganizationEmployeeDto.password,
      );
      return this.employeeRepository.save(createOrganizationEmployeeDto);
    } catch (ex) {
      throw new BadRequestException(`Create error: ${ex.message}`);
    }
  }

  async findAll(
    status: string | STATUSES,
    options: IPaginationOptions,
    sortOrder: SORT_ORDER,
    userId: string,
    type: string,
  ): Promise<Pagination<OrganizationEmployee>> {
    let condition = null;
    const queryBuilder = this.employeeRepository.createQueryBuilder('e');

    condition = organizationCondition(userId, type);

    if (condition) {
      queryBuilder.andWhere(condition);
    }

    if (status) {
      queryBuilder.andWhere({ status: status });
    }

    queryBuilder
      .leftJoinAndSelect('e.organization', 'o')
      .leftJoinAndSelect('e.user', 'u')
      .orderBy('e.employeeCode', sortOrder);

    return paginate<OrganizationEmployee>(queryBuilder, options);
  }

  async findOneByUsername(username: string, statuses?: STATUSES[]): Promise<OrganizationEmployee> {
    let condition = null;

    if (statuses) {
      condition = { status: In(statuses) };
    } else {
      condition = { status: Object.values(STATUSES)[STATUSES.ACTIVE] };
    }

    const employee = await this.employeeRepository.findOne({
      where: {
        username: username,
        ...condition,
      },
    });

    if (!employee) throw new UnauthorizedException('error.user not found');

    return employee;
  }

  async findOneById(userId: string, statuses?: STATUSES[]): Promise<OrganizationEmployee> {
    let condition = null;

    if (statuses) {
      condition = { status: In(statuses) };
    } else {
      condition = { status: Object.values(STATUSES)[STATUSES.ACTIVE] };
    }

    const employee = await this.employeeRepository.findOne({
      where: {
        id: userId,
        ...condition,
      },
    });

    if (!employee) throw new UnauthorizedException('error.user not found');

    return employee;
  }

  async findOne(id: number, userId: string, type: string): Promise<OrganizationEmployee> {
    let condition = null;
    condition = organizationCondition(userId, type);

    const employee = await this.employeeRepository.findOne({
      where: {
        id: id,
        ...condition,
      },
    });

    if (!employee) throw new UnauthorizedException('error.employee not found');

    return employee;
  }

  async update(
    id: number,
    updateOrganizationEmployeeDto: UpdateOrganizationEmployeeDto,
    userId: string,
    type: string,
  ): Promise<OrganizationEmployee | undefined> {
    if (!id) throw new BadRequestException('Update error: id is empty.');

    try {
      // Contact should belongs logged user
      const targetEmployee = await this.findOne(id, userId, type);
      if (!targetEmployee) {
        throw new BadRequestException('Update error: employee not belongs to organization');
      }

      updateOrganizationEmployeeDto.id = id;
      // if username exists check duplication
      if (updateOrganizationEmployeeDto.username) {
        const isUnique = await this.isUsernameUnique(
          updateOrganizationEmployeeDto.username,
          updateOrganizationEmployeeDto.id,
        );
        if (isUnique == false) {
          throw new BadRequestException('Username is duplicated');
        }
      }
      // if password exists encrypt
      if (updateOrganizationEmployeeDto.password) {
        updateOrganizationEmployeeDto.password = await passwordMd5Encrypt(
          updateOrganizationEmployeeDto.password,
        );
      }
      return this.employeeRepository.save(updateOrganizationEmployeeDto);
    } catch (ex) {
      throw new BadRequestException(`Update error: ${ex.message}`);
    }
  }

  async isUsernameUnique(username: string, id?: number): Promise<boolean> {
    let condition = null;

    // if record exits then expect own id
    if (id) {
      condition = { id: Not(id) };
    }

    const employee = await this.employeeRepository.findOne({
      where: {
        username: username,
        status: Object.values(STATUSES)[STATUSES.ACTIVE],
        ...condition,
      },
    });

    if (!employee) {
      return true;
    } else {
      return false;
    }
  }
}
