import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { OrganizationEmployeesService } from './organization-employees.service';
import { OrganizationEmployeesController } from './organization-employees.controller';
import { OrganizationEmployee } from './entities/organization-employee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([OrganizationEmployee])],
  controllers: [OrganizationEmployeesController],
  providers: [OrganizationEmployeesService],
  exports: [OrganizationEmployeesService],
})
export class OrganizationEmployeesModule {}
