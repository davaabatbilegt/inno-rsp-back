import { PartialType } from '@nestjs/swagger';

import { CreateOrganizationEmployeeDto } from './create-organization-employee.dto';

export class UpdateOrganizationEmployeeDto extends PartialType(CreateOrganizationEmployeeDto) {
  id: number;
}
