import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, Length } from 'class-validator';

import { STATUSES } from './../../utils/constants';

export class CreateOrganizationEmployeeDto {
  @ApiProperty({
    example: 1,
    required: false,
  })
  userId: number;

  @ApiProperty({
    example: 1,
    required: true,
  })
  organizationId: number;

  @ApiProperty({
    example: 'UMS-0000',
    required: false,
  })
  employeeCode: string;

  @ApiProperty({
    enum: STATUSES,
    example: `${Object.values(STATUSES).filter((v) => isNaN(Number(v)))}`,
    required: false,
  })
  status: string;

  @ApiProperty({
    example: 'testEmployee',
    required: true,
  })
  @IsNotEmpty({ message: 'Please enter username' })
  username: string;

  @ApiProperty({
    example: 'myPowefulPa$$001',
    required: true,
  })
  @IsNotEmpty({ message: 'Please enter password' })
  @Length(6, 50, { message: 'Password length Must be between 6 and 50 charcters' })
  password: string;
}
