import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';

import { STATUSES } from './../../utils/constants';

import { Organization } from './../../organizations/entities/organization.entity';
import { RestaurantOrder } from './../../restaurant-orders/entities/restaurant-order.entity';
import { RestaurantOrderDetail } from './../../restaurant-order-details/entities/restaurant-order-detail.entity';
import { User } from './../../users/entities/user.entity';

@Entity({ name: 'organization_employees' })
export class OrganizationEmployee {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ name: 'user_id' })
  userId: number;

  @Column({ name: 'organization_id' })
  organizationId: number;

  @Column({ name: 'employee_code' })
  employeeCode: string;

  @Column({
    type: 'enum',
    enum: STATUSES,
    default: STATUSES.ACTIVE,
    transformer: {
      to: (value: STATUSES) => STATUSES[value],
      from: (value: number) => STATUSES[value],
    },
  })
  status: string;

  @Column({ unique: true })
  username: string;

  @Exclude()
  @Column()
  password: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  // Many to One cascade
  @ManyToOne(() => Organization, (organization) => organization.contracts, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: 'organization_id' })
  organization: Organization;

  @ManyToOne(() => User, (user) => user.employees, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: 'user_id' })
  user: User;

  // One to Many cascade
  @OneToMany(() => RestaurantOrder, (order) => order.employee)
  orders: RestaurantOrder[];

  @OneToMany(() => RestaurantOrderDetail, (orderDetail) => orderDetail.employee)
  orderDetails: RestaurantOrderDetail[];

  constructor(partial: Partial<OrganizationEmployee>) {
    Object.assign(this, partial);
  }
}
