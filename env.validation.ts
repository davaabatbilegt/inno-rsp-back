import { plainToClass } from 'class-transformer';
import { IsEnum, IsNotEmpty, IsNumber, IsString, validateSync } from 'class-validator';
import { APP_ENV } from 'src/utils/constants';

export class EnvironmentVariables {
  @IsEnum(APP_ENV)
  @IsNotEmpty()
  APP_ENV: string;

  @IsString()
  @IsNotEmpty()
  DATABASE_HOST: string;

  @IsNumber()
  DATABASE_PORT: number;

  @IsString()
  @IsNotEmpty()
  DATABASE_USER: string;

  @IsString()
  @IsNotEmpty()
  DATABASE_PASSWORD: string;

  @IsString()
  @IsNotEmpty()
  DATABASE_DATABASE: string;

  // @IsString()
  // @IsNotEmpty()
  // GOOGLE_CLIENT_ID: string;

  // @IsString()
  // @IsNotEmpty()
  // GOOGLE_CLIENT_SECRET: string;

  @IsString()
  @IsNotEmpty()
  GOOGLE_REDIRECT_URI_ADMIN: string;

  @IsString()
  @IsNotEmpty()
  GOOGLE_REDIRECT_URI_CORPORATE: string;
}

export function validate(config: Record<string, unknown>) {
  const validatedConfig = plainToClass(EnvironmentVariables, config, {
    enableImplicitConversion: true,
  });
  const errors = validateSync(validatedConfig, {
    skipMissingProperties: false,
  });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }
  return validatedConfig;
}
