-- insert restaurant menu type -> Lunch set
INSERT INTO `rsp`.`restaurant_menu_types` (`id`, `name`, `description`) VALUES ('1', 'Lunch set', 'Lunch sets description');

-- insert admins
INSERT INTO `rsp`.`admins` (`name`, `username`, `password`) VALUES ('admin', 'admin', MD5('password'));

-- insert organizations
INSERT INTO `rsp`.`organizations` (`name`, `address`, `employee_count`, `description`, `username`, `password`, `salary_month_start_day`) VALUES ('Unimedia Solutions', 'Gerege Tower 11F', 70, 'IT company', 'ums', MD5('password'), 16);
INSERT INTO `rsp`.`organizations` (`name`, `address`, `employee_count`, `description`, `username`, `password`, `salary_month_start_day`) VALUES ('Unimedia', 'Tokyo, Japan', 50, 'Marketing company', 'um', MD5('password'), 16);

-- insert restaurants
INSERT INTO `rsp`.`restaurants` (`name`, `address`, `description`, `rating`, `username`, `password`) VALUES ('Cosy', 'NPC 1F', 'Cosy is located at NCP center', 4.5, 'cosy', MD5('password'));
INSERT INTO `rsp`.`restaurants` (`name`, `address`, `description`, `rating`, `username`, `password`) VALUES ('Ikh Mongol', 'Tsirkiin hajuud', 'Ikh Mongol has good beers', 3.75, 'ikhmongol', MD5('password'));

-- insert users
INSERT INTO `rsp`.`users` (`name`, `username`, `password`) VALUES ('Tamir', 'tamir', MD5('password'));
INSERT INTO `rsp`.`users` (`name`, `username`, `password`) VALUES ('Byambajargal', 'byamba', MD5('password'));
INSERT INTO `rsp`.`users` (`name`, `username`, `password`) VALUES ('Batbilegt', 'babi', MD5('password'));
INSERT INTO `rsp`.`users` (`name`, `username`, `password`) VALUES ('Uranchimeg', 'uraan', MD5('password'));
INSERT INTO `rsp`.`users` (`name`, `username`, `password`) VALUES ('Zagdsuren', 'zagaa', MD5('password'));

-- insert organization employees
INSERT INTO `rsp`.`organization_employees` (`user_id`, `organization_id`, `employee_code`, `username`, `password`) VALUES ('1', '1', 'UMS0155', 'tamir', MD5('password'));
INSERT INTO `rsp`.`organization_employees` (`user_id`, `organization_id`, `employee_code`, `username`, `password`) VALUES ('2', '1', 'UMS0013', 'byamba', MD5('password'));
INSERT INTO `rsp`.`organization_employees` (`user_id`, `organization_id`, `employee_code`, `username`, `password`) VALUES ('3', '1', 'UMS0176', 'babi', MD5('password'));
INSERT INTO `rsp`.`organization_employees` (`user_id`, `organization_id`, `employee_code`, `username`, `password`) VALUES ('4', '1', 'UMS0017', 'uraan', MD5('password'));
INSERT INTO `rsp`.`organization_employees` (`user_id`, `organization_id`, `employee_code`, `username`, `password`) VALUES ('5', '1', 'UMS0049', 'zagaa', MD5('password'));

-- insert restaurant menus
INSERT INTO `rsp`.`restaurant_menus` (`restaurant_id`, `restaurant_menu_type_id`, `name`, `description`, `price`) VALUES ('1', '1', 'BUUZ, HOTS BUDAATAI SHUL', 'description', '15000');
INSERT INTO `rsp`.`restaurant_menus` (`restaurant_id`, `restaurant_menu_type_id`, `name`, `description`, `price`, `days`) VALUES ('2', '1', 'Борш,Хуушуур', 'description', '14900', '1');
INSERT INTO `rsp`.`restaurant_menus` (`restaurant_id`, `restaurant_menu_type_id`, `name`, `description`, `price`, `days`) VALUES ('2', '1', 'Тахианы махтай цагаан гаатай шөл, Хонины шарсан хавирга', 'description', '14900', '2');
INSERT INTO `rsp`.`restaurant_menus` (`restaurant_id`, `restaurant_menu_type_id`, `name`, `description`, `price`, `days`) VALUES ('2', '1', 'Үхрийн махтай ногоотой шөл,гахайн шарсан мах', 'description', '14900', '3');
INSERT INTO `rsp`.`restaurant_menus` (`restaurant_id`, `restaurant_menu_type_id`, `name`, `description`, `price`, `days`) VALUES ('2', '1', 'Брокколи зутан шөл, гуляш', 'description', '14900', '4');
INSERT INTO `rsp`.`restaurant_menus` (`restaurant_id`, `restaurant_menu_type_id`, `name`, `description`, `price`, `days`) VALUES ('2', '1', 'Бинтэй хар шөл, терияаки тахиа', 'description', '14900', '5');
INSERT INTO `rsp`.`restaurant_menus` (`restaurant_id`, `restaurant_menu_type_id`, `name`, `description`, `price`) VALUES ('1', '1', 'teriyaki chicken,mushroom soup', 'description', '20000');

-- insert restaurant and organization contracts
INSERT INTO `rsp`.`restaurant_organization_contracts` (`restaurant_id`, `organization_id`, `restaurant_status`, `organization_status`, `menu_id`, `order_limit_time`, `order_limit_food_count`) VALUES ('1', '1', '1', '1', '1', '10:30', '20');
INSERT INTO `rsp`.`restaurant_organization_contracts` (`restaurant_id`, `organization_id`, `restaurant_status`, `organization_status`, `menu_id`, `order_limit_time`, `order_limit_food_count`) VALUES ('1', '2', '1', '1', '7', '11:30', '10');
INSERT INTO `rsp`.`restaurant_organization_contracts` (`restaurant_id`, `organization_id`, `restaurant_status`, `organization_status`, `menu_id`, `order_limit_time`, `order_limit_food_count`) VALUES ('2', '1', '1', '1', '2', '10:30', '20');
INSERT INTO `rsp`.`restaurant_organization_contracts` (`restaurant_id`, `organization_id`, `restaurant_status`, `organization_status`, `menu_id`, `order_limit_time`, `order_limit_food_count`) VALUES ('2', '1', '1', '1', '3', '10:30', '20');
INSERT INTO `rsp`.`restaurant_organization_contracts` (`restaurant_id`, `organization_id`, `restaurant_status`, `organization_status`, `menu_id`, `order_limit_time`, `order_limit_food_count`) VALUES ('2', '1', '1', '1', '4', '10:30', '20');
INSERT INTO `rsp`.`restaurant_organization_contracts` (`restaurant_id`, `organization_id`, `restaurant_status`, `organization_status`, `menu_id`, `order_limit_time`, `order_limit_food_count`) VALUES ('2', '1', '1', '1', '5', '10:30', '20');
INSERT INTO `rsp`.`restaurant_organization_contracts` (`restaurant_id`, `organization_id`, `restaurant_status`, `organization_status`, `menu_id`, `order_limit_time`, `order_limit_food_count`) VALUES ('2', '1', '1', '1', '6', '10:30', '20');

-- insert restaurant tables
INSERT INTO `rsp`.`restaurant_tables` (`restaurant_id`, `name`, `capacity`) VALUES ('1', 'cosy T1', '5');
INSERT INTO `rsp`.`restaurant_tables` (`restaurant_id`, `name`) VALUES ('1', 'cosy T2');
INSERT INTO `rsp`.`restaurant_tables` (`restaurant_id`, `name`) VALUES ('2', 'im T1');
INSERT INTO `rsp`.`restaurant_tables` (`restaurant_id`, `name`) VALUES ('2', 'im T2');