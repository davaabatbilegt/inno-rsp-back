-- Create local user
CREATE USER 'root'@'localhost' IDENTIFIED WITH authentication_plugin BY 'password';

-- Create database schema
CREATE DATABASE rsp CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;