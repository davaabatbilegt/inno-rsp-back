-- create admins table
CREATE TABLE `rsp`.`admins` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `status` INT DEFAULT 1,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create restaurants table
CREATE TABLE `rsp`.`restaurants` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `address` VARCHAR(255) NOT NULL,
  `description` VARCHAR(1024),
  `rating` DOUBLE DEFAULT 0,
  `latitude` DOUBLE DEFAULT NULL,
  `longitude` DOUBLE DEFAULT NULL,
  `capacity` INT DEFAULT NULL,
  `status` INT DEFAULT 1,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create organizations table
CREATE TABLE `rsp`.`organizations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `address` VARCHAR(255) NOT NULL,
  `employee_count` INT DEFAULT 1,
  `description` VARCHAR(1024),
  `status` INT DEFAULT 1,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `salary_month_start_day` INT DEFAULT 1,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create organization employees table
CREATE TABLE `rsp`.`organization_employees` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` INT,
  `organization_id` INT NOT NULL,
  `employee_code` VARCHAR(255),
  `status` INT DEFAULT 1,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create users table
CREATE TABLE `rsp`.`users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `status` INT DEFAULT 1,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create restaurant menu types table managed by admins
CREATE TABLE `rsp`.`restaurant_menu_types` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `status` INT DEFAULT 1,
  `description` VARCHAR(1023),
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create restaurant tables table managed by restaurants
CREATE TABLE `rsp`.`restaurant_tables` (
  `id` int NOT NULL AUTO_INCREMENT,
  `restaurant_id` INT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `status` INT DEFAULT 1,
  `capacity` INT DEFAULT 0,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create restaurant menus table managed by restaurants
CREATE TABLE `rsp`.`restaurant_menus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `restaurant_id` INT NOT NULL,
  `restaurant_menu_type_id` INT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `external_menu_id` INT DEFAULT 0,
  `status` INT DEFAULT 1,
  `description` VARCHAR(1023),
  `price` INT DEFAULT 0,
  `days` INT DEFAULT 0,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create restaurant onrganization contracts managed by restaurant, organziation
CREATE TABLE `rsp`.`restaurant_organization_contracts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `restaurant_id` INT NOT NULL,
  `organization_id` INT NOT NULL,
  `restaurant_status` INT DEFAULT 0,
  `organization_status` INT DEFAULT 0,
  `menu_id` INT NOT NULL,
  `menu_unit_price` INT NOT NULL DEFAULT 0,
  `status` INT DEFAULT 1,
  `order_limit_time` TIME DEFAULT '12:00:00',
  `order_limit_food_count` INT DEFAULT 0,
  `order_limit_food_money` INT DEFAULT 0,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create restaurant invoices table managed by restaurants, organizations
CREATE TABLE `rsp`.`restaurant_invoices` (
  `id` int NOT NULL AUTO_INCREMENT,
  `restaurant_id` INT NOT NULL,
  `organization_id` INT NOT NULL,
  `year_month` DATE NOT NULL,
  `status` INT DEFAULT 1,
  `bill_status` INT DEFAULT 0,
  `total_money` INT DEFAULT 0,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create restaurant invoice details table managed by restaurants, organizations
CREATE TABLE `rsp`.`restaurant_invoice_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `restaurant_id` INT NOT NULL,
  `restaurant_invoice_id` INT NOT NULL,
  `organization_id` INT NOT NULL,
  `year_month` DATE NOT NULL,
  `restaurant_menu_id` INT NOT NULL,
  `status` INT DEFAULT 1,
  `unit_count` INT DEFAULT 0,
  `unit_price` INT DEFAULT 0,
  `total_price` INT DEFAULT 0,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create restaurant orders table managed by restaurants, organization_employees, users
CREATE TABLE `rsp`.`restaurant_orders` (
  `id` int NOT NULL AUTO_INCREMENT,
  `restaurant_id` INT NOT NULL,
  `table_id` INT DEFAULT 0,
  `organization_id` INT,
  `employee_id` INT,
  `user_id` INT,
  `is_inside_contract_range` INT DEFAULT 0,
  `status` INT DEFAULT 1,
  `order_date` DATE DEFAULT (CURRENT_DATE),
  `order_status` INT DEFAULT 0,
  `total_bill` INT NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create restaurant order details table managed by restaurants, organization_employees, users
CREATE TABLE `rsp`.`restaurant_order_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `restaurant_id` INT NOT NULL,
  `restaurant_order_id` INT NOT NULL,
  `restaurant_menu_id` INT NOT NULL,
  `organization_id` INT,
  `employee_id` INT,
  `user_id` INT,
  `is_inside_contract_range` INT DEFAULT 0,
  `status` INT DEFAULT 1,
  `order_date` DATE DEFAULT (CURRENT_DATE),
  `order_status` INT DEFAULT 0,
  `unit_count` INT DEFAULT 0,
  `unit_price` INT DEFAULT 0,
  `total_price` INT DEFAULT 0,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;