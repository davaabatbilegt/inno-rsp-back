// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();
// eslint-disable-next-line @typescript-eslint/no-var-requires
const mysql = require('mysql2');

const args = process.argv.slice(2);
const DATABASE_DATABASE = process.env.DATABASE_DATABASE;
let queryString = '';

switch (args[0]) {
  case 'create':
    queryString = `CREATE SCHEMA IF NOT EXISTS \`${DATABASE_DATABASE}\` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;`;
    runQuery(queryString);
    break;
  case 'drop':
    queryString = `DROP SCHEMA IF EXISTS \`${DATABASE_DATABASE};\``;
    runQuery(queryString);
    break;
  default:
    console.log(`Run with parameter 'create' OR 'drop'`);
}

function runQuery(query) {
  const con = mysql.createConnection({
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    port: process.env.DATABASE_PORT,
  });

  con.connect(function (err) {
    if (err) throw err;
    con.query(query, function (err) {
      if (err) throw err;
      console.log(`Schema ${args[0]} success!`);
    });
    con.end();
  });
}
